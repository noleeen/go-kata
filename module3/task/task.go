package task

import (
	"encoding/json"
	"errors"
	"math/rand"
	"os"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type DoubleLinkedList struct {
	head *Node //начальный элемент списка
	tail *Node //последний элемент списка
	curr *Node //текукщий элемент, меняется при использовании методов next, prev
	len  int   //кол-во элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Push(c Commit)
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

//LOadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	file, err := os.ReadFile(path)
	if err != nil {
		panic("Fail reading json file")
	}
	obj := []Commit{}
	err = json.Unmarshal(file, &obj)
	if err != nil {
		panic("Fail unmarshaling data")
	}

	obj = QuickSort(obj)

	for _, v := range obj {
		d.Push(v)
	}

	return nil
}

func QuickSort(obj []Commit) []Commit {

	if len(obj) <= 1 {
		return obj
	}

	median := obj[rand.Intn(len(obj))].Date

	low_part := make([]Commit, 0, len(obj))
	high_part := make([]Commit, 0, len(obj))
	middle_part := make([]Commit, 0, len(obj))

	for _, item := range obj {
		switch {
		case item.Date.Before(median):
			low_part = append(low_part, item)
		case item.Date.Equal(median):
			middle_part = append(middle_part, item)
		case item.Date.After(median):
			high_part = append(high_part, item)
		}
	}

	low_part = QuickSort(low_part)
	high_part = QuickSort(high_part)

	low_part = append(low_part, middle_part...)
	low_part = append(low_part, high_part...)

	return low_part
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	return d.curr.next
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	return d.curr.prev
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if n > d.len-1 || n < -1 {
		return errors.New("error Insert: n out of range")
	}
	newNode := &Node{data: &c}

	switch {
	case d.len == 0:
		d.head = newNode
		d.tail = newNode
	case n == -1:
		newNode.next = d.head
		d.head.prev = newNode
		d.head = newNode
	case n == d.len-1:
		newNode.prev = d.tail
		d.tail.next = newNode
		d.tail = newNode
	default:
		index := 0
		currentNode := d.head
		for index != n {
			currentNode = currentNode.next
			index++
		}
		newNode.next = currentNode.next
		newNode.prev = currentNode
		currentNode.next.prev = newNode
		currentNode.next = newNode
	}
	d.len++
	return nil
}

// Push добавление элемента в конец списка
func (d *DoubleLinkedList) Push(c Commit) {
	newNode := &Node{data: &c}

	switch {
	case d.len == 0:
		d.head = newNode
		d.tail = newNode
	default:
		newNode.prev = d.tail
		d.tail.next = newNode
		d.tail = newNode
	}
	d.len++
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n < 0 || n > d.len-1 {
		return errors.New("error Delete: n out of range")
	}
	switch {
	case n == 0:
		if d.len == 1 {
			d.head = nil
		} else {
			d.head = d.head.next
			d.head.prev = nil
		}
	case n == d.len-1:
		d.tail = d.tail.prev
		d.head.next = nil
	default:
		index := 0
		currentNode := d.head
		for index != n {
			currentNode = currentNode.next
			index++
		}
		currentNode.prev.next = currentNode.next
		currentNode.next.prev = currentNode.prev
	}

	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return errors.New("error DeleteCurrent: current item is nill")
	}

	switch {
	case d.curr == d.head:
		if d.len == 1 {
			d.head = nil
		} else {
			d.head.next.prev = nil
			d.head.next = d.head
		}
	case d.curr == d.tail:
		d.tail.prev.next = nil
		d.tail.prev = d.tail
	case d.len == 1:
		d.tail = nil
		d.head = nil
		d.curr = nil
	default:
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
		d.curr = d.curr.next
	}
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.len == 0 {
		return -1, errors.New("error Index: linked list is empty")
	}
	tempNode := d.head
	index := 0
	for tempNode != d.curr {
		index++
		tempNode = tempNode.next
	}
	return index, nil
}

// Pop удаляет последний элемент списка и возвращает его значение
func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		panic("[pop]: linked list is empty")
	}

	temp := d.tail
	_ = d.Delete(d.len - 1)
	return temp
}

// Shift удаляет начальный элемент списка и возвращает его значение
func (d *DoubleLinkedList) Shift() *Node {
	if d.len == 0 {
		panic("[shift]: linked list is empty")
	}
	temp := d.head
	_ = d.Delete(0)
	return temp
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	if d == nil {
		panic("[SearchUUID]: linked list is nil")
	}

	tempNode := d.head
	for tempNode.data.UUID != uuID {
		if tempNode == nil {
			return nil
		}
		tempNode = tempNode.next
	}
	return tempNode
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	if d == nil {
		panic("[Search]: linked list is nil")
	}

	tempNode := d.head
	for tempNode.data.Message != message {
		if tempNode == nil {
			return nil
		}
		tempNode = tempNode.next
	}
	return tempNode
}

// Revers возвращает перевёрнутый список
func (d *DoubleLinkedList) Revers() *DoubleLinkedList {
	if d == nil {
		panic("[Revers]: linked list is nil")
	}
	if d.len == 0 || d.len == 1 {
		return d
	}
	count := d.len
	newList := DoubleLinkedList{}
	for i := 0; i < count; i++ {
		newList.Push(*d.Pop().data)
	}
	return &newList
}

func GenerateJSON(n int) error {
	newCommits := make([]Commit, n)
	for i := 0; i < n; i++ {
		newCommits[i].Message = gofakeit.Sentence(3)
		newCommits[i].UUID = gofakeit.UUID()
		newCommits[i].Date = gofakeit.Date()
	}
	jsonN, err := json.Marshal(newCommits)
	if err != nil {
		return errors.New("[GenerateJSON]: error Marshal")
	}
	err = os.WriteFile("generateJSON.json", jsonN, 0666)
	if err != nil {
		return errors.New("[GenerateJSON]: error WriteFile")
	}

	return nil
}

func GenerateCommits(n int) []Commit {
	newCommits := make([]Commit, n)
	for i := 0; i < n; i++ {
		newCommits[i].Message = gofakeit.Sentence(3)
		newCommits[i].UUID = gofakeit.UUID()
		newCommits[i].Date = gofakeit.Date()
	}
	return newCommits
}
