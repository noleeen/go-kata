package task

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDoubleLinkedList_LoadData(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")                                                // generateJSON.json - состоит из 7 элементов
	assert.Equal(t, obj.len, 7, "проверьте сколько в файле generateJSON.json элементов") // проверяем создался ли список длиною в 7 элементов
	assert.NoError(t, obj.LoadData("test.json"))                                         // проверяем не возникает ли ошибок при загрузки данных из тестового файла

}

func TestDoubleLinkedList_Len(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")

	assert.Equal(t, obj.Len(), obj.len)
}

func TestDoubleLinkedList_Current(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	obj.curr = obj.head
	assert.Equal(t, obj.Current(), obj.head)
}

func TestDoubleLinkedList_Next(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	obj.curr = obj.head
	assert.Equal(t, obj.Next(), obj.head.next)
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	obj.curr = obj.head.next
	assert.Equal(t, obj.Prev(), obj.head)
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	commit := GenerateCommits(1)[0]
	n := obj.len
	assert.NoError(t, obj.Insert(-1, commit)) //добавляем новый элемент в head
	assert.Error(t, obj.Insert(10, commit))   //ожидаем ошибку при попытке добавить элемент с индексом больше obj.len
	assert.Equal(t, obj.len, n+1)             //проверяем увеличилась ли длина списка на 1 при добавлении
	assert.Equal(t, *obj.head.data, commit)   // проверяем равен ли head коммиту который мы добавили
}

func TestDoubleLinkedList_Push(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	commit := GenerateCommits(1)[0]
	obj.Push(commit)
	assert.Equal(t, *obj.tail.data, commit) // проверяем равен ли последний элемент списка нашему запушенному элементу
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	n := obj.len
	head := obj.head
	assert.NoError(t, obj.Delete(0))   // удаляем начальный элемент списка
	assert.NotEqual(t, obj.head, head) // проверяем что теперь начальный элемент не равен, тому который был начальным
	assert.Equal(t, obj.len, n-1)      // проверяем уменьшилась ли длина списка на 1
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	n := obj.len
	headNext := obj.head.next
	obj.curr = obj.head.next                    // устанавливаем указатель на второй элемент списка
	assert.NoError(t, obj.DeleteCurrent())      // удаляем текущий элемент списка
	assert.NotEqual(t, obj.head.next, headNext) // проверяем что теперь второй элемент не равен, тому который был вторым
	assert.Equal(t, obj.len, n-1)               // проверяем уменьшилась ли длина списка на 1
}

func TestDoubleLinkedList_Index(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	obj.curr = obj.head.next // устанавливаем указатель на второй элемент списка(index == 1)
	ind, err := obj.Index()
	assert.Equal(t, ind, 1) // индекс второго элемента должен быть равен 1
	assert.NoError(t, err)

	obj2 := &DoubleLinkedList{}
	ind, err = obj2.Index()
	assert.Error(t, err) // ожидаем ошибку при пустом списке
	assert.Equal(t, ind, -1)
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	n := obj.len
	tail := obj.tail
	assert.Equal(t, obj.Pop(), tail)   // проверяем равен ли удаляемый элемент последнему элементу
	assert.NotEqual(t, obj.tail, tail) // проверяем что теперь последний элемент не равен, изначальному последнему
	assert.Equal(t, obj.len, n-1)      // проверяем уменьшилась ли длина списка на 1
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	n := obj.len
	head := obj.head
	assert.Equal(t, obj.Shift(), head) // проверяем равен ли удаляемый элемент первомуэлементу
	assert.NotEqual(t, obj.head, head) // проверяем что теперь первый элемент не равен, изначальному первому
	assert.Equal(t, obj.len, n-1)      // проверяем уменьшилась ли длина списка на 1
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	tailUUID := obj.tail.data.UUID
	assert.Equal(t, obj.SearchUUID(tailUUID), obj.tail) // проверяем равна ли нода которую мы ищем последней ноде
}

func TestDoubleLinkedList_Search(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")
	tailMessage := obj.tail.data.Message
	assert.Equal(t, obj.Search(tailMessage), obj.tail) // проверяем равна ли нода которую мы ищем последней ноде
}

func TestDoubleLinkedList_Revers(t *testing.T) {
	obj := &DoubleLinkedList{}
	_ = obj.LoadData("generateJSON.json")

	objLen := obj.len                 // сохраним длину исходного списка
	objHead := obj.head.data          //сохраним первый элемент исходного списка
	objHeadNext := obj.head.next.data // сохраним второй элемент исходного списка
	obj2 := obj.Revers()

	assert.Equal(t, obj2.tail.data, objHead)          // проверим равен ли последгий элемен нового списка первому элементу исходного списка
	assert.Equal(t, obj2.tail.prev.data, objHeadNext) // также сравним предпоследний со вторым
	assert.Equal(t, obj2.len, objLen)                 // проверяем равны ли длины исходного и полученного списков
}

func TestGenerateJSON(t *testing.T) {
	assert.NoError(t, GenerateJSON(7))

}
