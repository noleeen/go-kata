package main

import "fmt"

type PricingStrategy interface {
	Calculate(s Order) float64
}

type Order struct {
	Name   string
	Price  float64
	Amount int
}

type RegularPricing struct {
}

func (r RegularPricing) Calculate(s Order) float64 {
	return s.Price
}

type SalePricing struct {
	Name string
	Size float64

	//RegularCustomerDiscount float64
	//SpringDiscount float64
	//ChildDiscount float64
}

func (sale SalePricing) Calculate(s Order) float64 {
	return s.Price * sale.Size
}

func main() {
	Prices := []PricingStrategy{
		RegularPricing{},
		SalePricing{
			Name: "RegularCustomerDiscount",
			Size: 0.85,
		},
		SalePricing{
			Name: "SpringDiscount",
			Size: 0.9,
		},
		SalePricing{
			Name: "ChildDiscount",
			Size: 0.8,
		},
	}

	goodsShop1 := []Order{
		{
			Name:   "Book",
			Price:  40,
			Amount: 7,
		},
		{
			Name:   "Drink",
			Price:  10,
			Amount: 50,
		},
		{
			Name:   "Eat",
			Price:  15,
			Amount: 50,
		},
	}

	for _, v := range goodsShop1 {
		fmt.Printf("Total coast %s with %T strategy: %3.2f\n", v.Name, Prices[0], Prices[0].Calculate(v))
		fmt.Printf("Total coast %s with %#v strategy: %3.2f\n", v.Name, Prices[1], Prices[1].Calculate(v))
		fmt.Printf("Total coast %s with %#v strategy: %3.2f\n", v.Name, Prices[2], Prices[2].Calculate(v))
		fmt.Printf("Total coast %s with %#v strategy: %3.2f\n", v.Name, Prices[3], Prices[3].Calculate(v))
	}

}
