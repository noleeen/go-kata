package main

import (
	"fmt"
)

type AirConditionerProxy struct {
	adapter      *AirConditionerAdapter
	autenticated bool
	message      string
}

func NewAirConditionerProxy(autentifical bool) *AirConditionerProxy {
	return &AirConditionerProxy{adapter: &AirConditionerAdapter{&RealAirConditioner{}}, autenticated: autentifical}
}

func (a *AirConditionerProxy) TurnOn() {
	if a.autenticated {
		a.adapter.TurnOn()
		a.message = a.adapter.airConditioner.description
		InfoLogger.Println(a.message)
	} else {
		a.message = "you haven't permition"
		fmt.Println(a.message)
		InfoLogger.Println(a.message)

	}
}

func (a *AirConditionerProxy) TurnOff() {
	if a.autenticated {
		a.adapter.TurnOff()
		a.message = a.adapter.airConditioner.description
		InfoLogger.Println(a.message)
	} else {
		a.message = "you haven't permition"
		fmt.Println(a.message)
		InfoLogger.Println(a.message)
	}
}

func (a *AirConditionerProxy) SetTemperature(temp int) {
	if a.autenticated {
		a.adapter.SetTemperature(temp)
		a.message = a.adapter.airConditioner.description
		InfoLogger.Println(a.message)
	} else {
		a.message = "you haven't permition"
		fmt.Println(a.message)
		InfoLogger.Println(a.message)
	}
}
