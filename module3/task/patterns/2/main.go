package main

import (
	"fmt"
	"log"
	"os"
)

var InfoLogger *log.Logger

func init() {
	logConditioner, err := os.OpenFile("./module3/task/patterns/2/logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("[log | open file]", err)
	}
	//defer func() {
	//	_ = logConditioner.Close()
	//}()
	InfoLogger = log.New(logConditioner, "INFO_Conditioner_Use: ", log.Ldate|log.Ltime)

}

func main() {
	Lena := NewAirConditionerProxy(false)
	Egor := NewAirConditionerProxy(true)

	Lena.TurnOn()
	Lena.SetTemperature(40)
	Lena.TurnOff()
	fmt.Println()

	Egor.TurnOn()
	Egor.SetTemperature(21)
	Egor.TurnOff()

}
