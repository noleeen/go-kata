package main

type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}
