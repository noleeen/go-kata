package main

import "fmt"

type RealAirConditioner struct {
	isOn               bool
	currentTemperature int
	description        string
}

func (r *RealAirConditioner) TurnOn() {
	r.isOn = true
	r.description = "Turning on the air conditioner"
	fmt.Println(r.description)
}

func (r *RealAirConditioner) TurnOff() {
	r.isOn = false
	r.description = "Turning off the air conditioner"
	fmt.Println(r.description)
}

func (r *RealAirConditioner) SetTemperature(temp int) {
	r.currentTemperature = temp
	r.description = fmt.Sprintf("Setting air conditioner temperature to %d", temp)
	fmt.Println(r.description)
}
