package main

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (a *AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}
func (a *AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}
func (a *AirConditionerAdapter) SetTemperature(temp int) {
	a.airConditioner.SetTemperature(temp)
}
