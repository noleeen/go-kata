package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

type jsonWeather struct {
	Main struct {
		Temp     float64 `json:"temp"`
		Humidity int     `json:"humidity"`
	} `json:"main"`
	Wind struct {
		Speed float64 `json:"speed"`
	} `json:"wind"`
}

type OpenWeatherApi struct {
	apiKey      string
	weatherInfo *jsonWeather
}

func (o *OpenWeatherApi) GetWeatherInfo(location string) {
	url := "https://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=" + o.apiKey + "&units=metric"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println("[error http.NewRequest]: ", err)
	}
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		fmt.Println("[error client.Do(req)]: ", err)
	}
	defer func() {
		_ = response.Body.Close()
	}()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("[error io.ReadAll(response.Body)]: ", err)
	}
	//o.weatherInfo = jsonWeather{}
	if err := json.Unmarshal(body, &o.weatherInfo); err != nil {
		fmt.Println("[error json.Unmarshal]: ", err)
	}
}

func (o *OpenWeatherApi) GetTemperature(location string) int {
	o.GetWeatherInfo(location)
	return int(o.weatherInfo.Main.Temp)
}

func (o *OpenWeatherApi) GetHumidity(location string) int {
	o.GetWeatherInfo(location)
	return o.weatherInfo.Main.Humidity
}

func (o *OpenWeatherApi) GetWindSpeed(location string) int {
	o.GetWeatherInfo(location)
	return int(o.weatherInfo.Wind.Speed)
}

type WeatherFacade struct {
	wetherApi WeatherAPI
}

func (w *WeatherFacade) GetWetherInfo(location string) (int, int, int) {
	temperature := w.wetherApi.GetTemperature(location)
	humidity := w.wetherApi.GetHumidity(location)
	windSpeed := w.wetherApi.GetWindSpeed(location)
	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKeey string) WeatherFacade {
	return WeatherFacade{&OpenWeatherApi{apiKey: apiKeey}}
}

func main() {
	weatherFacade := NewWeatherFacade("6ac597fda32bae460c8e24717aabaeaa")
	cities := []string{"Минск", "Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWetherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n", windSpeed)
	}
}
