package main

import (
	"errors"
	"fmt"
	"log"
)

type WalletFacade struct {
	account      *Account
	wallet       *Wallet
	securityCode *SecurityCode
	notification *Notification
	ledger       *Ladger
}

func newWalletFacade(accountID string, code int) *WalletFacade {
	fmt.Println("Starting create account")
	w := &WalletFacade{
		account:      newAccount(accountID),
		wallet:       newWallet(),
		securityCode: newSecurityCode(code),
		notification: &Notification{},
		ledger:       &Ladger{},
	}
	fmt.Println("Account create")
	return w
}

func (w *WalletFacade) lookMoneytoWallet(account string, securityCode int) error {
	fmt.Println("Starting look balance")
	err := w.account.CheckAccount(account)
	if err != nil {
		return err
	}
	err = w.securityCode.CheckCode(securityCode)
	if err != nil {
		return err
	}

	w.wallet.lookWalletBalance()
	w.notification.LookBalanceNotification()
	w.ledger.makeEntry(account, "look balance", 0)

	return nil
}

func (w *WalletFacade) addMoneyToWallet(account string, securityCode, amount int) error {
	fmt.Println("Starting add money to wallet")
	err := w.account.CheckAccount(account)
	if err != nil {
		return err
	}

	err = w.securityCode.CheckCode(securityCode)
	if err != nil {
		return err
	}

	w.wallet.creditBalance(amount)
	w.notification.SendWalletCreditNotification()
	w.ledger.makeEntry(account, "credit", amount)
	return nil
}

func (w *WalletFacade) deductMoneyFromWallet(account string, securityCode, amount int) error {
	fmt.Println("Starting debit money")
	err := w.account.CheckAccount(account)
	if err != nil {
		return err
	}

	err = w.securityCode.CheckCode(securityCode)
	if err != nil {
		return err
	}

	err = w.wallet.debitBalance(amount)
	if err != nil {
		return err
	}
	w.notification.SendWalletDebitNotification()
	w.ledger.makeEntry(account, "debit", amount)

	return nil
}

type Account struct {
	name string
}

func newAccount(accountName string) *Account {
	return &Account{
		name: accountName,
	}
}

func (a *Account) CheckAccount(accountName string) error {
	if accountName != a.name {
		return fmt.Errorf("Account Name is incorrect")
	}
	fmt.Println("Account Verified")
	return nil
}

type SecurityCode struct {
	code int
}

func newSecurityCode(newCode int) *SecurityCode {
	return &SecurityCode{
		code: newCode,
	}
}

func (s *SecurityCode) CheckCode(code int) error {
	if code != s.code {
		return errors.New("Security code is incorrect")
	}
	fmt.Println("Security Code Verified")
	return nil
}

type Wallet struct {
	balance int
}

func newWallet() *Wallet {
	return &Wallet{balance: 0}
}

func (w *Wallet) lookWalletBalance() {
	fmt.Printf("Wallet balance: %d\n", w.balance)
}

func (w *Wallet) creditBalance(amount int) {
	w.balance = w.balance + amount
	fmt.Println("Wallet balance added successfully")

}

func (w *Wallet) debitBalance(amount int) error {
	if amount > w.balance {
		return fmt.Errorf("Balance is not sufficient")
	}
	fmt.Println("Wallet balance is sufficient")
	w.balance = w.balance - amount
	return nil
}

type Ladger struct {
}

func (l *Ladger) makeEntry(accountID, txnType string, amount int) {
	fmt.Printf("Make ledger entry for account %s with txnType %s for amount %d\n", accountID, txnType, amount)
	//return
}

type Notification struct {
}

func (n *Notification) SendWalletCreditNotification() {
	fmt.Println("Sending wallet credit notification")
}
func (n *Notification) SendWalletDebitNotification() {
	fmt.Println("Sending wallet debit notification")
}

func (n *Notification) LookBalanceNotification() {
	fmt.Println("Look wallet balance")
}

func main() {
	fmt.Println()
	wF := newWalletFacade("abc", 1234)

	fmt.Println()
	err := wF.addMoneyToWallet("abc", 1234, 10)
	if err != nil {
		log.Fatalf("Error: %s\n", err.Error())
	}

	fmt.Println()
	err = wF.lookMoneytoWallet("abc", 1234)
	if err != nil {
		log.Fatalf("Error:%s", err.Error())
	}

	fmt.Println()
	err = wF.deductMoneyFromWallet("abc", 1234, 5)
	if err != nil {
		log.Fatalf("Error: %s\n", err.Error())
	}

	fmt.Println()
	err = wF.lookMoneytoWallet("abc", 1234)
	if err != nil {
		log.Fatalf("Error:%s", err.Error())
	}

}
