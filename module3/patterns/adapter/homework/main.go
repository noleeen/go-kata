package main

import "fmt"

type falcon9Rocket struct {
	payload []interface{}
}

func (f9r *falcon9Rocket) insertSatelliteIntoStarlinkPort(s satellite) {
	fmt.Println("Attaching satellite to Falcon 9 Rocket.")
	s.insertSatelliteIntoStarlinkPort(f9r)
}

func (f9r falcon9Rocket) getPayLoad() {
	fmt.Printf("Falcon 9 payload: %s\n", f9r.payload)
}

type satellite interface {
	insertSatelliteIntoStarlinkPort(f9r *falcon9Rocket)
}

type starlinkSatellite struct {
	name string
}
type oco2Satellite struct {
	name string
}

func newStarlincSatellite() *starlinkSatellite {
	return &starlinkSatellite{
		name: "Starlink Satellite",
	}
}
func newOco2Satellite() *oco2Satellite {
	return &oco2Satellite{
		name: "Oco2 Satellite",
	}
}

func (sls *starlinkSatellite) insertSatelliteIntoStarlinkPort(f9r *falcon9Rocket) {
	f9r.payload = append(f9r.payload, sls)
	fmt.Println("Starlink satellite is attached to Falcon9 Rocket.")
}
func (oco *oco2Satellite) insertSatelliteIntoOco2Port(f9r *falcon9Rocket) {
	f9r.payload = append(f9r.payload, oco)
	fmt.Println("Oco2 satellite is attached to Falcon9 Rocket.")
}

type oco2SatelliteAdapter struct {
	oco2Satellite *oco2Satellite
}

func (ocoA oco2SatelliteAdapter) insertSatelliteIntoStarlinkPort(f9r *falcon9Rocket) {
	fmt.Println("Satellite adapter convert StarlincPort to Oco2Port.")
	ocoA.oco2Satellite.insertSatelliteIntoOco2Port(f9r)
}

func main() {
	rocket := &falcon9Rocket{}
	rocket.getPayLoad()

	satellite1 := newStarlincSatellite()
	rocket.insertSatelliteIntoStarlinkPort(satellite1)
	satellite2 := newOco2Satellite()
	satellite2Adapter := &oco2SatelliteAdapter{
		oco2Satellite: satellite2,
	}
	rocket.insertSatelliteIntoStarlinkPort(satellite2Adapter)

	rocket.getPayLoad()
}

/**
Последовательность:

Falcon 9 payload: []
Attaching satellite to Falcon 9 Rocket.
Starlink satellite is attached to Falcon 9 Rocket.
Attaching satellite to Falcon 9 Rocket.
Satellite adapter converts Starlink port to OCO2 port.
OCO2 satellite is attached to Falcon 9 Rocket.
Falcon 9 payload: [%!s(*main.starlinkSatellite=&{Starlink Satellite}) %!s(*main.oco2Satellite=&{OCO2 Satellite})]
**/
