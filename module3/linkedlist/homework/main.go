package main

import (
	"errors"
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64
	next        *Post
}

type Feed struct {
	length int
	start  *Post
	end    *Post
}

func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		f.end.next = newPost
		f.end = newPost
	}
	f.length++
}

// не могу понять почему не работает такая реализация: в текущий пост записываем следующий
//и в следующей итерации проверяем его по дате.
//линтер пишет мне что 49 строка никогда не будет выполнена. может подскажешь почему?
//func (f *Feed) Remove(publishDate int64) {
//	if f.length == 0 {
//		panic(errors.New("Feed is empty"))
//	}
//
//	currentPost := f.start
//
//	for currentPost.publishDate != publishDate {
//		if currentPost.next == nil {
//			panic(errors.New("No such Post found."))
//		}
//		currentPost = currentPost.next
//
//	}
//	currentPost = currentPost.next
//	f.length--
//}

func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic(errors.New("Feed is empty"))
	}

	var previousPost *Post
	currentPost := f.start

	for currentPost.publishDate != publishDate {
		if currentPost.next == nil {
			panic(errors.New("No such Post found."))
		}

		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next

	f.length--
}

func (f *Feed) Inspect() {
	currentPost := f.start
	fmt.Printf("\n Length feed: %d\n", f.length)
	for currentPost != f.end {
		fmt.Println(currentPost)
		currentPost = currentPost.next
	}
	fmt.Println(f.end)
}

func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
	} else {
		currentPost := f.start
		var previousPost *Post

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = currentPost.next
		}
		previousPost.next = newPost
		newPost.next = currentPost

	}
	f.length++

}

func main() {
	rightNow := time.Now().Unix()

	f := &Feed{}
	p1 := &Post{
		body:        "Lorem",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Lorem2",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "Lorem3",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "Lore4",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()

	newPost := &Post{
		body:        "New post",
		publishDate: rightNow + 15,
	}

	f.Insert(newPost)
	f.Inspect()
}
