package main

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/go-kata/module3/clean_architecture/service/service"
)

func main() {
	UserRepo := service.NewUserService(repo.NewFileRepository("UserRepo.json"))
	if err := UserRepo.R.InitJsonRepository(); err != nil {
		fmt.Println(err)
	}

	run := true
	for run {
		item := 0
		fmt.Println("\nSelect item:\n" +
			"1. Show list tasks\n" +
			"2. Create task\n" +
			"3. Change status: Processing\n" +
			"4. Change status: Complete\n" +
			"5. Remove task\n" +
			"6. Exit\n" +
			"Enter item number: ")
		_, err := fmt.Scan(&item)
		if err != nil {
			fmt.Println(err)
		}

		switch item {
		case 1:
			tasks, err := UserRepo.List()
			if err != nil {
				fmt.Println(err)
			}
			for _, v := range tasks {
				fmt.Printf("ID: %d | Title: %s | Discription: %s | Status: %s \n", v.ID, v.Title, v.Description, v.Status)
			}
		case 2:
			fmt.Println("Enter title of task ")
			//title, err := bufio.NewReader(os.Stdin).ReadString('.')
			//if err != nil {
			//	fmt.Println(err)
			//}
			title := bufio.NewScanner(os.Stdin)
			title.Scan()
			if err := UserRepo.Create(title.Text()); err != nil {
				fmt.Println(err)
			}
		case 3:
			var id int
			fmt.Println("Enter id of task for status `processing` ")
			_, err = fmt.Scan(&id)
			if err != nil {
				fmt.Println(err)
			}
			if err := UserRepo.Processing(id); err != nil {
				fmt.Println(err)
			}
		case 4:
			var id int
			fmt.Println("Enter id of task for status `complete` ")
			_, err = fmt.Scan(&id)
			if err != nil {
				fmt.Println(err)
			}
			if err := UserRepo.Complete(id); err != nil {
				fmt.Println(err)
			}
		case 5:
			var id int
			fmt.Println("Enter id of task for remove")
			_, err = fmt.Scan(&id)
			if err != nil {
				fmt.Println(err)
			}
			if err := UserRepo.Remove(id); err != nil {
				fmt.Println(err)
			}
		case 6:
			run = false
		case 7:
			fmt.Println("-------\n*** congratulation!***\nyou chose the secret item, \nso you will meet Great Luck tomorrow!\n------- ")
		default:
			fmt.Println("incorrect input! try again")
		}

	}

}
