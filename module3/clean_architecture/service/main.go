package main

import (
	"fmt"

	"gitlab.com/go-kata/module3/clean_architecture/service/model"

	"gitlab.com/go-kata/module3/clean_architecture/service/repo"
)

func main() {
	rep := repo.NewFileRepository("./module3/clean_architecture/service/p.json")
	_ = rep.InitJsonRepository()

	m1 := rep.NewTask("First")
	_, err := rep.Create(m1)
	if err != nil {
		fmt.Println(err)
	}

	m := make([]model.Task, 7)
	for i := 0; i < len(m); i++ {
		title := fmt.Sprintf("title%d", i)
		m[i] = rep.NewTask(title)
		fmt.Println(m[i])
	}
	fmt.Println(m, m[0])
	fmt.Printf("type %T", m[0])

}
