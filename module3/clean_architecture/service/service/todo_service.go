package service

import (
	"fmt"

	"gitlab.com/go-kata/module3/clean_architecture/service/model"
)

type Repository interface {
	InitJsonRepository() error
	NewTask(title string) model.Task
	GetTasks() ([]model.Task, error)
	GetTask(id int) (model.Task, error)
	Create(task model.Task) (model.Task, error)
	Save(tasks []model.Task) error
	Update(task model.Task) (model.Task, error)
	Delete(id int) error
}

type TodoService interface {
	List() ([]model.Task, error)
	Create(title string) error
	Processing(id int) error
	Complete(id int) error
	Remove(id int) error
}

type UserService struct {
	R Repository
}

func NewUserService(rep Repository) *UserService {
	return &UserService{R: rep}
}

func (t *UserService) List() ([]model.Task, error) {
	return t.R.GetTasks()

}

func (t *UserService) Create(title string) error {
	task := t.R.NewTask(title)
	_, err := t.R.Create(task)
	if err != nil {
		return fmt.Errorf("[service | create]: %s", err)
	}
	return nil
}

func (t *UserService) Processing(id int) error {
	tasks, err := t.List()
	if err != nil {
		return fmt.Errorf("[service | processing]: %s", err)
	}
	for i, v := range tasks {
		if v.ID == id {
			tasks[i].Status = "Processing"

			_, err = t.R.Update(tasks[i])
			if err != nil {
				return fmt.Errorf("[service | processing]: %s", err)
			}
			return nil
		}
	}
	return fmt.Errorf("[service | processing]: task with id %d was not found", id)
}

func (t *UserService) Complete(id int) error {
	tasks, err := t.List()
	if err != nil {
		return fmt.Errorf("[service | processing]: %s", err)
	}

	for i, v := range tasks {
		if v.ID == id {
			tasks[i].Status = "Complete"

			_, err = t.R.Update(tasks[i])
			if err != nil {
				return fmt.Errorf("[service | complete]: %s", err)
			}
			return nil
		}
	}
	return fmt.Errorf("[service | complete]: task with id %d was not found", id)
}

func (t *UserService) Remove(id int) error {
	if err := t.R.Delete(id); err != nil {
		return fmt.Errorf("[service | remove]: %s", err)
	}
	return nil
}
