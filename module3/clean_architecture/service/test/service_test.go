package test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/go-kata/module3/clean_architecture/service/service"
)

func TestServiceAll(t *testing.T) {
	testRep2 := repo.NewFileRepository("./p2.json")
	testService := service.NewUserService(testRep2)

	m := make([]model.Task, 3)
	for i := 0; i < len(m); i++ {
		title := fmt.Sprintf("title%d", i)
		m[i] = testService.R.NewTask(title)
		m[i].ID = i + 1
	}

	err := testService.R.InitJsonRepository()
	assert.NoError(t, err)

	err = testService.Create(m[0].Title)
	assert.NoError(t, err)

	err = testService.Create(m[1].Title)
	assert.NoError(t, err)

	err = testService.Create(m[2].Title)
	assert.NoError(t, err)

	arr, err := testService.List()
	assert.NoError(t, err)
	assert.Equal(t, len(arr), 3) // проверяем добавились ли все 3 элемента в репозиторий

	err = testService.Processing(1)
	assert.NoError(t, err)
	arr, err = testService.List()
	assert.NoError(t, err)
	assert.Equal(t, arr[0].Status, "Processing") // проверяем поменялся ли статус

	err = testService.Complete(1)
	assert.NoError(t, err)
	arr, err = testService.List()
	assert.NoError(t, err)
	assert.Equal(t, arr[0].Status, "Complete") // проверяем поменялся ли статус

	err = testService.Remove(3)
	assert.NoError(t, err)
	arr, err = testService.List()
	assert.NoError(t, err)
	assert.Equal(t, len(arr), 2) // проверяем уменьшился ли список элементов
}
