package test

import (
	"fmt"
	"testing"

	"gitlab.com/go-kata/module3/clean_architecture/service/model"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-kata/module3/clean_architecture/service/repo"
)

func TestRepositoryAll(t *testing.T) {
	testRep := repo.NewFileRepository("./p.json")

	m := make([]model.Task, 3)
	for i := 0; i < len(m); i++ {
		title := fmt.Sprintf("title%d", i)
		m[i] = testRep.NewTask(title)
		m[i].ID = i + 1
	}

	err := testRep.InitJsonRepository()

	assert.NoError(t, err)

	_, err = testRep.Create(m[0])
	assert.NoError(t, err)

	_, err = testRep.Create(m[1])
	assert.NoError(t, err)

	_, err = testRep.Create(m[2])
	assert.NoError(t, err)

	arr, err := testRep.GetTasks()
	assert.NoError(t, err)
	assert.Equal(t, len(arr), 3) // проверяем добавились ли все 3 элемента в репозиторий

	arg, err := testRep.GetTask(1)
	assert.NoError(t, err)
	assert.Equal(t, arg, m[0]) // проверяем равен ли первый элемент репозитория, первому элементу, который мы записывали

	m[0].Title = "NewTitle"
	arg, err = testRep.Update(m[0])
	assert.NoError(t, err)
	assert.Equal(t, arg.Title, m[0].Title)

	arg, err = testRep.GetTask(1)
	assert.NoError(t, err)
	assert.Equal(t, arg.Title, m[0].Title) //проверяем действительно ли в репозитории изменились данные после Update

	err = testRep.Delete(3)
	assert.NoError(t, err)
	_, err = testRep.GetTask(3)
	assert.NoError(t, err) //ожидаем отсутствия ошибки, но появления оповещения, что элемента с таким индексом не найдено
}
