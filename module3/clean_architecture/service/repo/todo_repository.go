package repo

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/go-kata/module3/clean_architecture/service/model"
)

type FileRepository struct {
	Path string
}

func NewFileRepository(path string) *FileRepository {
	return &FileRepository{Path: path}
}

func (r *FileRepository) NewTask(title string) model.Task {
	return model.Task{
		Title:       title,
		Description: "new task",
		Status:      "need processing",
	}
}

func (r *FileRepository) InitJsonRepository() error {
	tasks := make([]model.Task, 0)
	if err := r.Save(tasks); err != nil {
		return err
	}
	return nil
}

func (r *FileRepository) GetTasks() ([]model.Task, error) {
	if _, err := os.Stat(r.Path); os.IsNotExist(err) {
		fmt.Printf("[Get tasks]: file %s is not exist\n", r.Path)
		return nil, nil
	}

	var tasks []model.Task
	//file, err := os.Open(r.Path)
	//if err != nil{
	//	return nil, fmt.Errorf("[Get tasks]: %s", err)
	//}
	//defer func() {
	//	_ = file.Close()
	//}()

	content, err := os.ReadFile(r.Path)
	if err != nil {
		return nil, fmt.Errorf("[Get tasks | read file]: %s", err)
	}

	if err = json.Unmarshal(content, &tasks); err != nil {
		return nil, fmt.Errorf("[Get tasks | unmarshal]: %s", err)
	}
	return tasks, nil
}

func (r *FileRepository) GetTask(id int) (model.Task, error) {
	var task model.Task

	tasks, err := r.GetTasks()
	if err != nil {
		return task, err
	}

	for _, v := range tasks {
		if v.ID == id {
			return v, nil
		}
	}
	fmt.Printf("task with id %d was not found", id)

	return task, nil
}

func (r *FileRepository) Create(task model.Task) (model.Task, error) {
	tasks, err := r.GetTasks()
	if err != nil {
		return task, err
	}
	task.ID = len(tasks) + 1
	tasks = append(tasks, task)

	if err := r.Save(tasks); err != nil {
		return task, err
	}
	return task, nil
}

func (r *FileRepository) Save(tasks []model.Task) error {
	m, err := json.Marshal(tasks)
	if err != nil {
		return fmt.Errorf("[Save | marshal]: %s", err)
	}
	err = os.WriteFile(r.Path, m, 0644)
	if err != nil {
		return fmt.Errorf("[Save | write file]: %s", err)
	}
	return nil
}

func (r *FileRepository) Update(task model.Task) (model.Task, error) {
	tasks, err := r.GetTasks()
	if err != nil {
		return task, err
	}
	for i, v := range tasks {
		if v.ID == task.ID {
			tasks[i] = task
		}
	}

	if err := r.Save(tasks); err != nil {
		return task, err
	}
	return task, nil
}

func (r *FileRepository) Delete(id int) error {
	tasks, err := r.GetTasks()
	if err != nil {
		return err
	}
	lenTemp := len(tasks)
	for i, v := range tasks {
		if v.ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...)
		}
	}
	if lenTemp == len(tasks) {
		return fmt.Errorf("[Delete]: task with id %d was not found", id)
	}

	if err := r.Save(tasks); err != nil {
		return err
	}

	return nil
}
