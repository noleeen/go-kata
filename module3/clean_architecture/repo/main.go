package main

import (
	"fmt"

	"gitlab.com/go-kata/module3/clean_architecture/repo/repository"
)

const (
	path = "module3/clean_architecture/repo/testData.json"
)

func main() {
	r := repository.NewUserRepository(path)
	err := r.JsonInit()
	fmt.Println(err)

	u4 := repository.NewUser(4, "WorkPlease")
	u5 := repository.NewUser(5, "ButWhy")

	fmt.Println(r.LenList())
	err = r.Save(*u4)
	fmt.Println(err)
	err = r.Save(*u5)
	fmt.Println(err)
	fmt.Println(r.Find(4))
	fmt.Println(r.LenList())
	fmt.Println(r.FindAll())
}
