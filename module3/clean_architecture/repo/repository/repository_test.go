package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUserRepository_All(t *testing.T) {

	uR := NewUserRepository("path.json")
	u1 := NewUser(1, "Sasha")
	u2 := NewUser(2, "Aryna")
	u3 := NewUser(3, "Alesya")

	assert.NoError(t, uR.JsonInit())
	assert.NoError(t, uR.Save(*u1))
	assert.Equal(t, uR.LenList(), 1)
	assert.NoError(t, uR.Save(*u2))
	assert.NoError(t, uR.Save(*u3))
	assert.Equal(t, uR.LenList(), 3)

}
