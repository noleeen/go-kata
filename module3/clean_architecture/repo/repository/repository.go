package repository

import (
	"encoding/json"
	"errors"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
	ReadUsers() ([]User, error)
	JsonInit() error
}

type UserRepository struct {
	//File  io.ReadWriter
	FilePath   string
	CountUsers int
}

func NewUserRepository(path string) *UserRepository {
	return &UserRepository{
		FilePath: path,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (r *UserRepository) JsonInit() error { // создаёт json файл и записывает в него 0-го юзера с именем admin
	admin := make([]User, 1)
	admin[0] = *NewUser(0, "admin2")

	m, err := json.MarshalIndent(admin, "", " ")
	if err != nil {
		return errors.New("[jsoninit]: error marshal")
	}
	err = os.WriteFile(r.FilePath, m, 0666)
	if err != nil {
		return errors.New("[jsoninit]: error write file")
	}
	return nil
}

func NewUser(id int, name string) *User {
	nUser := User{
		ID:   id,
		Name: name,
	}
	return &nUser
}

func (r *UserRepository) LenList() int {
	return r.CountUsers
}

func (r *UserRepository) Save(record interface{}) error {

	users, err := r.ReadUsers()
	if err != nil {
		err = r.JsonInit()
		if err != nil {
			return errors.New("sdfafasdgfasdga")
		}
		//return errors.New("[save]: error readusers")
	}

	u, ok := record.(User)
	if !ok {
		return errors.New("[save]: error input argument type's")
	}
	for _, v := range users {
		if v.ID == u.ID {
			return errors.New("[save]: error - this ID already exists")
		}
	}
	users = append(users, u)

	usersMarshal, err := json.MarshalIndent(users, "", " ")
	if err != nil {
		return errors.New("[save]: error marshal users")
	}
	err = os.WriteFile(r.FilePath, usersMarshal, 0644)
	if err != nil {
		return errors.New("[save]: error writefile")
	}
	r.CountUsers++
	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	users, err := r.ReadUsers()
	if err != nil {
		return nil, errors.New("[find]: error readusers")
	}
	for _, v := range users {
		if v.ID == id {
			//fmt.Println(v)
			return v, nil
		}
	}
	return nil, errors.New("[find]: id not found")
}
func (r *UserRepository) FindAll() (interface{}, error) {
	users, err := r.ReadUsers()
	if err != nil {
		return nil, errors.New("[find]: error readusers")
	}
	//for _, v := range users {
	//	fmt.Println(v)
	//}
	return users, nil
}

func (r *UserRepository) ReadUsers() ([]User, error) {
	data, err := os.ReadFile(r.FilePath)
	if err != nil {
		return nil, errors.New("[readusers]: error readfile")
	}
	users := make([]User, 0, r.CountUsers)
	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, errors.New("[readusers]: error unmarshal")
	}
	return users, nil
}
