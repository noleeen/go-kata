package medium

//type ListNode struct{
//	Val int
//	Next *ListNode
//}

//https://leetcode.com/problems/merge-in-between-linked-lists/

// 1вариант: когда a и b это значение узлов:
//func mergeInBetween(list1 *ListNode, a,b int, list2 *ListNode) *ListNode{
//	currentNode := list1
//	var previousNode *ListNode
//
//	for currentNode.Val != a {
//		previousNode = currentNode
//		currentNode = currentNode.Next
//	}
//	currentNode2 := currentNode
//	for currentNode2.Val != b{
//		currentNode2 = currentNode2.Next
//	}
//
//
//	previousNode.Next = list2
//	currentNode = previousNode.Next
//	for currentNode.Next != nil{
//		currentNode = currentNode.Next
//	}
//	currentNode.Next = currentNode2.Next
//
//	return list1
//}

// 2вариант: когда a и b это номер определённого узла начиная с ноля:
func mergeInBetween(list1 *ListNode, a, b int, list2 *ListNode) *ListNode { //nolint: all
	currentNode := list1
	var previousNode *ListNode
	i := 0
	for i = 0; i < a; i++ {
		previousNode = currentNode
		currentNode = currentNode.Next
	}
	temp := currentNode
	for j := i; j < b; j++ {
		temp = temp.Next
	}

	previousNode.Next = list2
	currentNode = list2
	for currentNode.Next != nil {
		currentNode = currentNode.Next
	}
	currentNode.Next = temp.Next

	return list1
}
