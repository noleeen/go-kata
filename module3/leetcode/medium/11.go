package medium

func removeLeafNodes(root *TreeNode, target int) *TreeNode { //nolint: all
	if root == nil {
		return nil
	}
	root.Left = removeLeafNodes(root.Left, target)
	root.Right = removeLeafNodes(root.Right, target)

	if root.Right == nil && root.Left == nil && root.Val == target {
		root = nil
	}
	return root
}
