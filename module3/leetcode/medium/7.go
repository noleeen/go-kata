package medium

type TreeNode8 struct {
	Val   int
	Left  *TreeNode8
	Right *TreeNode8
}

//балансировка с помощью создания слайса из узлов дерева
func balanceBST(root *TreeNode8) *TreeNode8 { //nolint: all
	var arr []*TreeNode8
	createSlice(root, &arr)
	return balanceTreeFromSlice(arr)

}

func balanceTreeFromSlice(arr []*TreeNode8) *TreeNode8 { //nolint: all
	lenArr := len(arr)
	if lenArr == 0 {
		return nil
	}
	if lenArr == 1 {
		arr[0].Left, arr[0].Right = nil, nil
		return arr[0]
	}
	midleNode := arr[lenArr/2]
	midleNode.Left = balanceTreeFromSlice(arr[:lenArr/2])
	midleNode.Right = balanceTreeFromSlice(arr[lenArr/2+1:])
	return midleNode
}

func createSlice(root *TreeNode8, nodes *[]*TreeNode8) { //nolint: all
	if root != nil {
		createSlice(root.Left, nodes)
		*nodes = append(*nodes, root)
		createSlice(root.Right, nodes)
	}

}
