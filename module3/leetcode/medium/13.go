package medium

func maxSum(grid [][]int) int { //nolint: all
	maxS := 0
	sumClock := 0
	for i := 1; i < len(grid)-1; i++ {
		for j := 1; j < len(grid[i])-1; j++ {
			sumClock = grid[i][j] + grid[i-1][j-1] + grid[i-1][j] + grid[i-1][j+1] + grid[i+1][j-1] + grid[i+1][j] + grid[i+1][j+1]
			if sumClock > maxS {
				maxS = sumClock
			}
		}
	}
	return maxS
}
