package medium

type SubrectangleQueries struct {
	rect [][]int
}

func Constructor(rectangle [][]int) SubrectangleQueries {
	return SubrectangleQueries{rect: rectangle}
}

func (this *SubrectangleQueries) UpdateSubrectangle(row1, col1, row2, col2, newValue int) {
	for i := row1; i <= row2; i++ {
		for j := col1; j <= col2; j++ {
			this.rect[i][j] = newValue
		}
	}
}

func (this *SubrectangleQueries) GetValue(row, col int) int {
	return this.rect[row][col]
}
