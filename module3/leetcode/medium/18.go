package medium

func groupThePeople(groupSizes []int) [][]int { //nolint: all
	listOfGroups := [][]int{}

	for i, v := range groupSizes {
		temp := []int{}
		if v >= 0 {
			temp = append(temp, i)
			for j := i + 1; j < len(groupSizes); j++ {
				if v == groupSizes[j] {
					if len(temp) == v {
						listOfGroups = append(listOfGroups, temp)
						temp = []int{}
					}
					temp = append(temp, j)
					groupSizes[j] *= -1
				}
			}
			if len(temp) > 0 {
				listOfGroups = append(listOfGroups, temp)
			}
		}
	}
	return listOfGroups
}
