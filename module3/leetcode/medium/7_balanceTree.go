package medium

// ДОПИЛИТЬ НЕ ВСЕ ТЕСТЫ ПРОХОДИТ https://leetcode.com/problems/balance-a-binary-search-tree/

type TreeNode2 struct {
	Val   int
	Left  *TreeNode2
	Right *TreeNode2
}

const (
	L = 1
	R = 0
)

func balanceBST2(root *TreeNode2) *TreeNode2 { //nolint: all
	if root == nil {
		return nil
	}
	diff := 2
	for diff > 1 {
		leftHeigth := checkDepth(root.Left)
		rightHeigth := checkDepth(root.Right)

		diff = leftHeigth - rightHeigth
		if diff < 0 {
			diff *= -1
		}
		if diff > 1 && leftHeigth > rightHeigth {
			root = turnTree(root, R)
		} else if diff > 1 && rightHeigth > leftHeigth {
			root = turnTree(root, L)
		}
	}
	return root
}

func turnTree(root *TreeNode2, side int) *TreeNode2 { //nolint: all
	switch side {
	case L:
		newRoot := root.Right
		leftHeight := checkDepth(newRoot.Left)
		rightHeight := checkDepth(newRoot.Right)

		//сначала правый поворот потом левый
		if leftHeight > rightHeight {
			root.Right = newRoot.Left
			newRoot.Left = newRoot.Left.Right
			root.Right.Right = newRoot
			newRoot = root.Right
		}

		// простой левый поворот
		root.Right = newRoot.Left
		newRoot.Left = root
		root = newRoot
	case R:
		newRoot := root.Left
		leftHeight := checkDepth(newRoot.Left)
		rightHeight := checkDepth(newRoot.Right)

		if rightHeight > leftHeight {
			root.Left = newRoot.Right
			newRoot.Right = newRoot.Right.Left
			root.Left.Left = newRoot
			newRoot = root.Left
		}

		// простой правый поворот
		root.Left = newRoot.Right
		newRoot.Right = root
		root = newRoot
	}

	return root
}

func checkDepth(node *TreeNode2) int { //nolint: all
	if node == nil {
		return 0
	}
	left := checkDepth(node.Left)
	right := checkDepth(node.Right)

	if left > right {
		return left + 1
	}
	return right + 1
}
