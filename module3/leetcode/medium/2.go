package medium

import "sort"

func sortTheStudents(score [][]int, k int) [][]int { //nolint: all
	sort.Slice(score, func(i, j int) bool {
		return score[i][k] > score[j][k]
	})
	return score

}
