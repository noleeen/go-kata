package medium

type TreeNode5 struct {
	Val   int
	Left  *TreeNode5
	Right *TreeNode5
}

func bstToGst(root *TreeNode5) *TreeNode5 { //nolint: all
	reverseBranch(root, 0)
	return root
}

func reverseBranch(root *TreeNode5, sum int) int { //nolint: all
	if root.Right != nil {
		sum = reverseBranch(root.Right, sum)
	}
	root.Val += sum
	sum = root.Val

	if root.Left != nil {
		sum = reverseBranch(root.Left, sum)
	}
	return sum
}
