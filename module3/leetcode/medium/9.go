package medium

import "sort"

func checkAritmeticsSubarrays(nums []int, l []int, r []int) []bool { //nolint: all
	boolList := make([]bool, 0, len(l))
	for i := 0; i < len(l); i++ {
		tempList := make([]int, 0, len(nums)/2)
		for j := l[i]; j <= r[i]; j++ {
			tempList = append(tempList, nums[j])
		}
		sort.Ints(tempList)
		check := true
		for k := 0; k < len(tempList)-1; k++ {
			if tempList[k+1]-tempList[k] != tempList[1]-tempList[0] {
				check = false
				break
			}
		}
		boolList = append(boolList, check)

	}
	return boolList
}
