package medium

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int { //nolint: all
	sum := 0
	if root == nil {
		return 0
	}
	if root.Right == nil && root.Left == nil {
		return root.Val
	}

	level := make([]*TreeNode, 0, 1)
	level = append(level, root)
	lastLevel := true
	for lastLevel {
		currentSum := 0
		childLevel := make([]*TreeNode, 0, len(level)*2)
		for _, v := range level {
			if v != nil {
				childLevel = append(childLevel, v.Left, v.Right)
			}
		}
		lastLevel = false
		for _, v := range childLevel {
			if v != nil {
				currentSum += v.Val
				lastLevel = true
			}
		}
		if lastLevel {
			sum = currentSum
			level = childLevel
		}
	}
	return sum
}

//вариант2
//func deepestLeavesSum(root *TreeNode) int {
//	mapDeep := make(map[int]int)
//	deepSum(root, mapDeep, 0)
//	return mapDeep[len(mapDeep)-1]
//}
//
//func deepSum(root *TreeNode, m map[int]int, deep int) {
//	m[deep] += root.Val
//
//	if root.Left != nil {
//		deepSum(root.Left, m, deep+1)
//	}
//	if root.Right != nil {
//		deepSum(root.Right, m, deep+1)
//	}
//}
