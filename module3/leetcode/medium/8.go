package medium

func findSmallestSetOfVertices(n int, edges [][]int) []int { //nolint: all
	//создаём слайс длиной равной количеству узлов n. затем первым циклом отмечаем единицей те индексы(вершины)
	//в которых есть входящие рёбра.  индексы, которые не отметятся и будут ответом
	//(т е находим узлы, в которые нет входящих рёбер)
	arr := make([]int, n)
	var res []int

	for _, v := range edges {
		arr[v[1]] = 1
	}

	for i, v := range arr {
		if v == 0 {
			res = append(res, i)
		}
	}
	return res

}
