package medium

func numTilePossibilities(tiles string) int { //nolint: all
	resMap := make(map[string]struct{})
	symbols := make([]string, 0)

	for i := 0; i < len(tiles); i++ {
		symbol := string(tiles[i])
		resMap[symbol] = struct{}{}
		lenSymbols := len(symbols)
		symbols = append(symbols, symbol)
		for j := 0; j < lenSymbols; j++ {
			for k := 0; k <= len(symbols[j]); k++ {
				el := symbols[j]
				el = el[:k] + symbol + el[k:]
				resMap[el] = struct{}{}
				symbols = append(symbols, el)
			}
		}
	}
	return len(resMap)
}
