package medium

type TreeNode3 struct {
	Val   int
	Left  *TreeNode3
	Right *TreeNode3
}

func constructMaximumBinaryTree(nums []int) *TreeNode3 { //nolint: all
	if len(nums) == 0 {
		return nil
	}

	root := &TreeNode3{}
	i := 0
	root.Val, i = max(nums)

	root.Left = constructMaximumBinaryTree(nums[:i])
	root.Right = constructMaximumBinaryTree(nums[i+1:])

	return root
}

func max(n []int) (int, int) { //nolint: all
	max := 0
	ind := 0
	for i, v := range n {
		if v > max {
			max = v
			ind = i
		}
	}
	return max, ind
}
