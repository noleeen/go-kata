package medium

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode { //nolint: all
	res := &ListNode{}
	tail := res
	for n := head.Next; n.Next != nil; n = n.Next {
		if n.Val == 0 {
			tail.Next = &ListNode{Val: 0}
			tail = tail.Next
		} else {
			tail.Val += n.Val
		}
	}
	return res
}
