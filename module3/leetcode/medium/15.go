package medium

import "strconv"

func minPartitions(n string) int { //nolint: all
	res := 0
	max := n[0]
	for i := 1; i < len(n); i++ {
		if n[i] > max {
			max = n[i]
		}
	}
	res, _ = strconv.Atoi(string(max))
	return res
}
