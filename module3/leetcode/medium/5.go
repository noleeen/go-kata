package medium

type ListNode2 struct {
	Val  int
	Next *ListNode2
}

func pairSum(head *ListNode2) int { //nolint: all
	arr := []int{}
	max := 0
	cur := head
	for cur != nil {
		arr = append(arr, cur.Val)
		cur = cur.Next
	}
	for i := 0; i < len(arr)/2; i++ {
		if (arr[i] + arr[len(arr)-1-i]) > max {
			max = arr[i] + arr[len(arr)-1-i]
		}
	}
	return max
}
