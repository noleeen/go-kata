package easy

import "fmt"

func uniqueMorseRepresentations(words []string) int { //nolint: all
	morzeMap := map[rune]string{'a': ".-", 'b': "-...", 'c': "-.-.", 'd': "-..", 'e': ".", 'f': "..-.", 'g': "--.", 'h': "....",
		'i': "..", 'j': ".---", 'k': "-.-", 'l': ".-..", 'm': "--", 'n': "-.", 'o': "---", 'p': ".--.", 'q': "--.-", 'r': ".-.", 's': "...",
		't': "-", 'u': "..-", 'v': "...-", 'w': ".--", 'x': "-..-", 'y': "-.--", 'z': "--.."}
	count := len(words)

	resM := make([]string, len(words))
	for i, v := range words {

		for _, m := range v {
			resM[i] += morzeMap[m]
		}

		for j := i - 1; j >= 0; j-- {
			if resM[i] == resM[j] {
				count--
				break
			}
		}
		fmt.Println(resM[0], resM[1], resM[2], resM[3])
	}
	return count
}
