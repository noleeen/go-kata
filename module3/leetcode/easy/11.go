package easy

func runningSum(nums []int) []int { //nolint: all
	sumArr := make([]int, 0, len(nums))
	sum := 0
	for _, v := range nums {
		sum += v
		sumArr = append(sumArr, sum)
	}
	return sumArr
}
