package easy

//
func numberOfMatches(n int) int { //nolint: all
	amountMatches := 0
	for n > 1 {
		matches := n / 2
		if n%2 != 0 {
			n = n/2 + 1
		} else {
			n = n / 2
		}
		amountMatches += matches
	}
	return amountMatches
}
