package easy

func convertTemperature(celsius float64) [2]float64 { //nolint:all
	Kelvin := celsius + 273.15
	Fahrenheit := celsius*1.80 + 32.00
	temperature := [2]float64{Kelvin, Fahrenheit}
	return temperature
}
