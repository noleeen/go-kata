package easy

func buildArray(nums []int) []int { //nolint: all
	ans := make([]int, len(nums))
	for i := range nums {
		ans[i] = nums[nums[i]]
	}
	return ans

}
