package easy

type ParkingSystem struct {
	big    int
	medium int
	small  int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	n := ParkingSystem{
		big:    big,
		medium: medium,
		small:  small,
	}
	return n
}

func (p *ParkingSystem) AddCar(carType int) bool { // nolint: all
	switch carType {
	case 1:
		if p.big > 0 {
			p.big--
			return true
		} else {
			return false
		}
	case 2:
		if p.medium > 0 {
			p.medium--
			return true
		} else {
			return false
		}
	case 3:
		if p.small > 0 {
			p.small--
			return true
		} else {
			return false
		}
	default:
		return false
	}
}
