package easy

func minimumSum(num int) int { //nolint: all
	arr := make([]int, 0, 4)
	for num > 0 {
		arr = append(arr, num%10)
		num = num / 10
	}

	for i := 0; i < 3; i++ {
		for j := 0; j < len(arr)-i; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}
	return (arr[0]*10 + arr[2]) + (arr[1]*10 + arr[3])
}
