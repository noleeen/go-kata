package easy

import "strings"

func mostWordsFound(sentences []string) int { // nolint: all
	res := 0
	for _, v := range sentences {
		b := strings.Split(v, " ")
		if len(b) > res {
			res = len(b)
		}
	}
	return res
}
