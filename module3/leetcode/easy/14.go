package easy

func maximumWealth(accounts [][]int) int { //nolint:all
	maxWhealth := 0

	for _, v := range accounts {
		whealth := 0
		for _, w := range v {
			whealth += w
		}
		if whealth > maxWhealth {
			maxWhealth = whealth

		}
	}
	return maxWhealth
}
