package easy

func kidsWithCandies(candies []int, extraCandies int) []bool { //nolint: all
	max := 0
	resArr := make([]bool, len(candies))
	for _, v := range candies {
		if v > max {
			max = v
		}
	}
	for i, v := range candies {
		if v+extraCandies >= max {
			resArr[i] = true
		} else {
			resArr[i] = false
		}
	}
	return resArr
}
