package easy

func tribonacci(n int) int { //nolint:all
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	}
	t1 := 0
	t2 := 1
	t3 := 1
	var temp int
	for i := 3; i <= n; i++ {
		temp = t3
		t3 = t1 + t2 + t3
		t1 = t2
		t2 = temp
	}

	return t3
}
