package easy

func nunmJewelsIsStones(jewels, stones string) int { // nolint: all
	count := 0
	for _, v := range jewels {
		for _, v2 := range stones {
			if v == v2 {
				count++
			}
		}
	}
	return count
}
