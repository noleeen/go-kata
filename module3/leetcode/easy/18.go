package easy

func differenceOfSum(nums []int) int { //nolint: all
	sumNums := 0
	sumDigits := 0
	for _, v := range nums {
		sumNums += v
		for v > 0 {
			sumDigits += v % 10
			v = v / 10
		}
	}
	return sumNums - sumDigits
}
