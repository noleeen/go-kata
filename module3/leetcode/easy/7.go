package easy

import "strings"

func defangIPaddr(address string) string { //nolint: all
	return strings.ReplaceAll(address, ".", "[.]")
}
