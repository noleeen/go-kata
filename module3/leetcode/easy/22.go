package easy

func smallerNumbersThanCurrent(nums []int) []int { //nolint: all
	res := make([]int, len(nums))

	for i, v := range nums {
		count := 0
		for j := 0; j < len(nums); j++ {
			if v > nums[j] {
				count++
			}
		}
		res[i] = count
	}
	return res
}
