package easy

func findKthPositive(arr []int, k int) int { //nolint: all
	tempArr := make([]int, 0, k)
	count := 0
	if arr[0] != 1 {
		j := 1
		for arr[0] > j {
			tempArr = append(tempArr, j)
			count++
			j++
		}
	}
	for i := 0; count < k; i++ {
		j := 1
		if i < len(arr)-1 {
			for arr[i+1] > arr[i]+j {
				tempArr = append(tempArr, arr[i]+j)
				count++
				j++
			}
		} else {
			ost := k - count
			for n := 1; n <= ost; n++ {
				tempArr = append(tempArr, arr[len(arr)-1]+n)
				count++

			}
		}
	}

	return tempArr[k-1]
}
