package easy

func createTargetArray(nums []int, index []int) []int { // nolint: all
	res := make([]int, len(index))
	for i, vI := range index {
		res = append(res[:vI], append([]int{nums[i]}, res[vI:len(index)-1]...)...)
	}

	return res
}
