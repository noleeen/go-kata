package easy

func smallestEvenMultiple(n int) int { //nolint: all
	if n%2 == 0 {
		return n
	} else {
		return n * 2
	}
}
