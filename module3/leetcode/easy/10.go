package easy

func shuffle(nums []int, n int) []int { //nolint: all
	resArr := make([]int, 0, len(nums))
	for i := 0; i < len(nums)/2; i++ {
		resArr = append(resArr, nums[i])
		resArr = append(resArr, nums[len(nums)/2+i])
	}
	return resArr
}
