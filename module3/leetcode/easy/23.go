package easy

func interpret(command string) string { // nolint: all
	comMap := map[string]string{"G": "G", "()": "o", "(al)": "al"}
	resStr := ""
	a := ""
	for i := 0; i < len(command); i++ {
		a += string(command[i])
		if a == "G" || a == "()" || a == "(al)" {
			resStr += comMap[a]
			a = ""
		}
	}
	return resStr
}
