package main

import "fmt"

type Stack []string

//IsEmpty: check if steck empty
func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

//Push a new value onto the stack
func (s *Stack) Push(str string) {
	*s = append(*s, str)
}

//Remove and return top element of stack. Return false if stack is empty
func (s *Stack) Pop() (string, bool) {
	if s.IsEmpty() {
		return "", false
	} else {
		index := len(*s) - 1
		elem := (*s)[index]
		*s = (*s)[:index]
		return elem, true
	}
}

func main() {
	var staack Stack
	staack.Push("this")
	staack.Push("is")
	staack.Push("sparta!)")
	fmt.Println(staack)

	for len(staack) > 0 {
		x, y := staack.Pop()
		if y {
			fmt.Println(x)
		}
	}
	fmt.Println(staack.IsEmpty())
}
