package main

import "fmt"

func BubbleSort(arr *[6]int) [6]int {

	for j := 1; j < len(arr); j++ {

		for i := 0; i < len(arr)-j; i++ {
			if arr[i] > arr[i+1] {
				arr[i], arr[i+1] = arr[i+1], arr[i]
			}
		}
	}
	return *arr
}

func main() {
	b := [6]int{5, 7, 1, 33, 77, 1}
	BubbleSort(&b)
	//fmt.Println(BubbleSort(b))
	fmt.Println(b)
}
