package main

import (
	"errors"
	"fmt"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	t := &TreeNode{Val: 8}
	fmt.Println(t)

	_ = t.Insert(5)
	_ = t.Insert(10)
	_ = t.Insert(9)
	_ = t.Insert(4)
	_ = t.Insert(6)

	fmt.Println(t.Right)
	fmt.Println(t.Left.Left)
	fmt.Println(t.Find(10))

	t.Delete(9)
	t.PrintInOrder()

	fmt.Println("min", t.FindMin())
	fmt.Println("max", t.FindMin())

	link := []*TreeNode{t}
	fmt.Println(link[0])
	link[0].PrintInOrder()
}

//prints the elements in order
func (t *TreeNode) PrintInOrder() {
	if t == nil {
		//fmt.Println("tree is empty")
		return
	}
	t.Left.PrintInOrder()
	fmt.Println(t.Val)
	t.Right.PrintInOrder()
}

//insert a new node itno the binary tree while adhearing to the rules of a perffect BST
func (t *TreeNode) Insert(value int) error {
	if t == nil {
		return errors.New("tree is nil")
	}
	if t.Val == value {
		return errors.New("this node value already exists")
	}

	if t.Val > value {
		if t.Left == nil {
			t.Left = &TreeNode{Val: value}
			return nil
		}
		return t.Left.Insert(value)
	}

	if t.Val < value {
		if t.Right == nil {
			t.Right = &TreeNode{Val: value}
			return nil
		}
		return t.Right.Insert(value)
	}
	return nil
}

//find the treenode for the given node val
func (t *TreeNode) Find(v int) (TreeNode, bool) {
	if t == nil {
		return TreeNode{}, false
	}
	switch {
	case v == t.Val:
		return *t, true
	case v < t.Val:
		return t.Left.Find(v)
	default:
		return t.Right.Find(v)
	}
}

//Delete removes the Item with value from the tree
func (t *TreeNode) Delete(v int) {
	t.remove(v)
}

func (t *TreeNode) remove(v int) *TreeNode {
	if t == nil {
		return nil
	}

	if v < t.Val {
		t.Left = t.Left.remove(v)
		return t
	}

	if v > t.Val {
		t.Right = t.Right.remove(v)
		return t
	}

	if t.Left == nil && t.Right == nil {
		t = nil
		return nil
	}

	if t.Left == nil {
		t = t.Right
		return t
	}

	if t.Right == nil {
		t = t.Left
		return t

	}
	smalestValOnRight := t.Right
	for {
		///find smalest value on the right side
		if smalestValOnRight != nil && smalestValOnRight.Left != nil {
			smalestValOnRight = smalestValOnRight.Left
		} else {
			break
		}
	}
	t.Val = smalestValOnRight.Val
	t.Right = t.Right.remove(v)
	return t
}

func (t *TreeNode) FindMax() int {
	if t.Right == nil {
		return t.Val
	}
	return t.Right.FindMax()
}

func (t *TreeNode) FindMin() int {
	if t.Left == nil {
		return t.Val
	}
	return t.Left.FindMin()
}
