package main

import "fmt"

type Node struct {
	val   int
	left  *Node
	right *Node
}

func NewNode(val int) *Node {
	return &Node{val: val}
}

func (n *Node) Insert(val int) {
	if val == n.val {
		fmt.Printf("node with value %d has existed yet\n", val)
		return
	}

	if val < n.val {
		if n.left == nil {
			n.left = NewNode(val)
		} else {
			n.left.Insert(val)
		}
	} else {
		if n.right == nil {
			n.right = NewNode(val)
		} else {
			n.right.Insert(val)
		}
	}
}

func (n *Node) Print() {
	if n == nil {
		return
	}
	n.left.Print()
	fmt.Println(n.val)
	n.right.Print()
}

func (n *Node) Print2() {
	if n == nil {
		return
	}
	fmt.Println(n.val)
	n.left.Print()
	n.right.Print()

}

func main() {
	n := NewNode(10)
	n.Insert(5)
	n.Insert(1)
	n.Insert(3)
	n.Insert(7)
	n.Insert(9)
	n.Print()
	fmt.Println()
	n.Print2()
}
