package algo

func BubbleSort(data []int) []int {
	for j := 1; j < len(data); j++ {

		for i := 0; i < len(data)-j; i++ {
			if data[i] > data[i+1] {
				data[i], data[i+1] = data[i+1], data[i]
			}
		}
	}

	return data
}

func BubbleSortSwap(d []int) []int {
	swapped := true
	i := 1
	for swapped {
		swapped = false
		for j := 0; j < len(d)-i; j++ {
			if d[i] > d[i+1] {
				d[i], d[i+1] = d[i+1], d[i]
				swapped = true
			}
		}
		i++
	}
	return d

}
