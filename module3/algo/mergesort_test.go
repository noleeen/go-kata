package algo

import (
	"reflect"
	"sort"
	"testing"
)

func Test_mergeSort(t *testing.T) {
	data := RandData(100, 1000)
	sorted := append([]int{}, data()...)
	sort.Ints(sorted)
	type args struct {
		arr []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort reversed slice",
			args: args{arr: []int{55, 34, 21, 13, 8, 5, 3}},
			want: []int{3, 5, 8, 13, 21, 34, 55},
		},
		{
			name: "sort 1000 numbers",
			args: args{arr: data()},
			want: sorted,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MergeSort(tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("mergeSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkMergeSort(b *testing.B) {
	dataSet := GenerateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MergeSort(dataSet[i])
	}
}
