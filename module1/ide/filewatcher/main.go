package main

import (
	"fmt"
	"math/rand"
	"os"
)

const (
	noError = iota
	generalError
)

func printime(text string) {
	fmt.Println(text)
}

func main() {
	printime("I will can")
	roll := rand.Intn(100)
	if roll > 50 {
		os.Exit(noError)
	} else {
		os.Exit(generalError)
	}
}
