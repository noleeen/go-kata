package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	port := ":8080"
	srv := &http.Server{
		Addr:    port,
		Handler: http.HandlerFunc(handler),
	}
	//запуск веб-сервера в отдельной горутине
	go func() {
		log.Println("server started onm port", port)
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal("listen: ", err)
		}
	}()

	// ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown server...")

	//установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server shutdown:", err)
	}
	log.Println("Server exiting")
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "today is good day")
}
