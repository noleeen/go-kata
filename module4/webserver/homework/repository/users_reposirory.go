package repository

import (
	"errors"
	"fmt"

	"gitlab.com/go-kata/module4/webserver/homework/domain/entity"
	"gitlab.com/go-kata/module4/webserver/homework/domain/service"
)

type usersRepository struct {
	users []entity.User
}

func NewStorage() service.UsersRepository {
	users := []entity.User{
		{ID: 1, Name: "Alesya", Description: "doctor"},
		{ID: 2, Name: "Aryna", Description: "violin"},
		{ID: 3, Name: "Aleksandra", Description: "strategy"},
	}
	return &usersRepository{users}
}

func (r *usersRepository) GetUsers() []entity.User {
	return r.users
}

func (r *usersRepository) GetUser(id int) (entity.User, error) {
	for _, v := range r.users {
		if v.ID == id {
			return v, nil
		}
	}
	return entity.User{}, fmt.Errorf("user with ID: %d was not found", id)
}

func (r *usersRepository) NewUser(user entity.User) (int, error) {
	if user.ID != 0 {
		return 0, errors.New("don't write ID. ID assigned automatically")
	}
	user.ID = len(r.users) + 1
	r.users = append(r.users, user)

	return user.ID, nil
}
