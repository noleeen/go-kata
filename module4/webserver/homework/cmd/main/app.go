package main

import (
	"context"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/go-kata/module4/webserver/homework/domain/service"
	"gitlab.com/go-kata/module4/webserver/homework/handlers"
	"gitlab.com/go-kata/module4/webserver/homework/pkg/logging"
	"gitlab.com/go-kata/module4/webserver/homework/repository"
)

func main() {
	logger := logging.GetLogger()
	logger.Info("create router")
	router := httprouter.New()
	rep := repository.NewStorage()

	logger.Info("register user handler")
	handler := handlers.NewHandler(*service.NewService(rep), logger)
	handler.Register(router)

	server := &http.Server{
		Handler:      router,
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
	}

	//запуск веб-сервера в отдельной горутине
	go start(router, server)

	//механизм graceful shutdown для корректной остановки сервера
	GracefulShutdown(server, logger)

}

func start(router *httprouter.Router, server *http.Server) {
	logger := logging.GetLogger()
	logger.Info("start application")
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		panic(err)
	}

	logger.Info("server is listening port 0.0.0.0:1234")
	logger.Fatal(server.Serve(listener))
}

func GracefulShutdown(server *http.Server, logger logging.Logger) {
	//ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	logger.Info("shutdown server")

	//установка таймаута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		logger.Fatal("Server Shutdown:", err)
	}
	logger.Info("Server exiting")

}
