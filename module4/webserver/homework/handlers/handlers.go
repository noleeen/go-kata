package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/go-kata/module4/webserver/homework/pkg/logging"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/go-kata/module4/webserver/homework/domain/entity"
	"gitlab.com/go-kata/module4/webserver/homework/domain/service"
)

type handler struct {
	rep    service.UserService
	logger logging.Logger
}

func NewHandler(service service.UserService, logger logging.Logger) Handler {
	return &handler{
		rep:    service,
		logger: logger,
	}
}

func (h *handler) Register(router *httprouter.Router) {
	router.GET("/", h.Welcome)
	router.GET("/users", h.GetAllUsers)
	router.GET("/users/:id", h.GetUserForId)
	router.POST("/users", h.CreateNewUser)
	router.POST("/upload", h.Upload)
	router.GET("/public/filename.txt", h.GetFileFromServer)
}

func (h *handler) Welcome(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	h.logger.Info("Welcome")
	_, err := fmt.Fprint(w, "Welcome human! Today is good day for important deals!\n")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *handler) GetAllUsers(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	h.logger.Info("GetAllUsers")
	resp, err := json.Marshal(h.rep.GetUsers())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = w.Write(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *handler) GetUserForId(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	h.logger.Info("GetUserForId")
	id, err := strconv.Atoi(params.ByName("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user, err := h.rep.GetUser(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	_, err = fmt.Fprint(w, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *handler) CreateNewUser(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	h.logger.Info("CreateNewUser")
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var user entity.User
	if err := json.Unmarshal(reqBody, &user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	id, err := h.rep.NewUser(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = fmt.Fprintf(w, "New user with ID: %d added successfully\n", id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *handler) Upload(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	h.logger.Info("Upload")
	file, header, err := r.FormFile("filename")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()

	fileOnServer, err := os.Create("./public/" + header.Filename)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	defer fileOnServer.Close()

	_, err = io.Copy(fileOnServer, file)
	if err != nil {
		return
	}

	_, err = fmt.Fprintf(w, "file %s uploaded successfully\n", header.Filename)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *handler) GetFileFromServer(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	h.logger.Info("GetFileServer")
	http.ServeFile(w, r, r.URL.Path)
}
