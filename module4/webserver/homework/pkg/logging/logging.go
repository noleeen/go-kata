package logging

import (
	"fmt"
	"io"
	"os"
	"path"
	"runtime"

	"github.com/sirupsen/logrus"
)

//-------------------------
type writerHook struct {
	Writer   []io.Writer
	LogLavel []logrus.Level
}

func (hook *writerHook) Fire(entry *logrus.Entry) error {
	line, err := entry.String()
	if err != nil {
		return err
	}
	for _, v := range hook.Writer {
		v.Write([]byte(line)) //nolint :all
	}
	return nil
}
func (hook *writerHook) Levels() []logrus.Level {
	return hook.LogLavel
}

//------------------------------

//------------------------------
var e *logrus.Entry

type Logger struct {
	*logrus.Entry
}

func GetLogger() Logger {
	return Logger{e}
}
func (l *Logger) GetLoggerWithField(k string, v interface{}) Logger { //метод для возможности создания ещё одного логгера с определённым полем
	return Logger{l.WithField(k, v)}
}

func init() {
	l := logrus.New()
	l.SetReportCaller(true)
	l.Formatter = &logrus.TextFormatter{
		CallerPrettyfier: func(frame *runtime.Frame) (function string, file string) {
			filename := path.Base(frame.File)
			return fmt.Sprintf("%s()", frame.Function), fmt.Sprintf("%s:%d", filename, frame.Line)
		},
		DisableColors: false,
		FullTimestamp: true,
	}

	err := os.MkdirAll("./module4/webserver/homework/pkg/logs", 0744)
	if err != nil {
		panic(err)
		//return
	}
	allFile, err := os.OpenFile("./module4/webserver/homework/pkg/logs/all.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0640)
	if err != nil {
		panic(err)
		//return
	}
	l.SetOutput(io.Discard) //говорим логгеру чтоб он ничего не писал, так как в ручную с помощью хуков реализуем запись в два места

	l.AddHook(&writerHook{
		Writer:   []io.Writer{allFile, os.Stdout},
		LogLavel: logrus.AllLevels,
	})
	l.SetLevel(logrus.TraceLevel)

	e = logrus.NewEntry(l)

}
