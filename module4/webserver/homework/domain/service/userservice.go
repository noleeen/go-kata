package service

import "gitlab.com/go-kata/module4/webserver/homework/domain/entity"

type UsersRepository interface {
	GetUsers() []entity.User
	GetUser(id int) (entity.User, error)
	NewUser(user entity.User) (int, error)
}

type UserService struct {
	rep UsersRepository
}

func NewService(r UsersRepository) *UserService {
	return &UserService{rep: r}
}

func (s *UserService) GetUsers() []entity.User {
	return s.rep.GetUsers()
}

func (s *UserService) GetUser(id int) (entity.User, error) {
	return s.rep.GetUser(id)
}

func (s *UserService) NewUser(user entity.User) (int, error) {
	return s.rep.NewUser(user)
}
