package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/go-kata/module4/webserver/swagger/internal/transport"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func main() {
	port := ":8080"
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	petHandler := transport.GetHandlerPet()
	swaggerHandler := transport.GetHandlerSwagger()
	orderHandler := transport.GetHandlerOrder()
	userHandler := transport.GetHandlerUser()

	petHandler.Register(r)
	swaggerHandler.Register(r)
	orderHandler.Register(r)
	userHandler.Register(r)

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	//запуск веб-сервера в отдельной горутине
	go func() {
		log.Printf("server started on port %s \n", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	//ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server...")

	//установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server shutdown:", err)
	}
	log.Println("Server exiting")
}
