package service

import (
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/repository"
)

type PetStorager interface {
	Create(pet entity.Pet) entity.Pet
	Update(pet entity.Pet) (entity.Pet, error)
	Delete(petId int) error
	GetById(petId int) (entity.Pet, error)
	GetList() []entity.Pet
	UploadImage(petID int, url string) (entity.Pet, error)
}

type PetService struct {
	rep PetStorager
}

func NewPetService() *PetService {
	return &PetService{rep: repository.NewPetStorage()}
}

func (p *PetService) ServiceCreate(pet entity.Pet) entity.Pet {
	return p.rep.Create(pet)
}

func (p *PetService) ServiceUpdate(pet entity.Pet) (entity.Pet, error) {
	return p.rep.Update(pet)
}

func (p *PetService) ServiceDelete(petId int) error {
	return p.rep.Delete(petId)
}

func (p *PetService) ServiceGetById(petId int) (entity.Pet, error) {
	return p.rep.GetById(petId)
}

func (p *PetService) ServiceGetList() []entity.Pet {
	return p.rep.GetList()
}

func (p *PetService) ServiceUploadImage(petID int, url string) (entity.Pet, error) {
	return p.rep.UploadImage(petID, url)
}
