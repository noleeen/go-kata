package service

import (
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/repository"
)

type UserStorager interface {
	Create(user entity.User) (entity.User, error)
	Find(username string) (entity.User, error)
	Delete(username string) error
	Update(username string, user entity.User) (entity.User, error)
}

type UserService struct {
	rep UserStorager
}

func NewUserService() *UserService {
	return &UserService{rep: repository.NewUserStorage()}
}

func (u *UserService) ServiceUserCreate(user entity.User) (entity.User, error) {
	return u.rep.Create(user)
}

func (u *UserService) ServiceUserFind(username string) (entity.User, error) {
	return u.rep.Find(username)
}

func (u *UserService) ServiceUserDelete(username string) error {
	return u.rep.Delete(username)
}

func (u *UserService) ServiceUserUpdate(username string, user entity.User) (entity.User, error) {
	return u.rep.Update(username, user)
}
