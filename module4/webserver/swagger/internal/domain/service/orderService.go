package service

import (
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/repository"
)

type OrderStorager interface {
	Create(order entity.Order) entity.Order
	Find(orderId int) (entity.Order, error)
	Delete(orderId int) error
	ListStatusCodes() map[string]int
}

type OrderService struct {
	rep OrderStorager
}

func NewOrderService() *OrderService {
	return &OrderService{rep: repository.NewOrderStorage()}
}

func (o *OrderService) ServiceOrderCreate(order entity.Order) entity.Order {
	return o.rep.Create(order)
}

func (o *OrderService) ServiceOrderFind(orderId int) (entity.Order, error) {
	return o.rep.Find(orderId)
}

func (o *OrderService) ServiceOrderDelete(orderId int) error {
	return o.rep.Delete(orderId)
}

func (o *OrderService) ServiceOrderListStatusCodes() map[string]int {
	return o.rep.ListStatusCodes()
}
