package repository

import (
	"fmt"
	"sync"

	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
)

type UserStorage struct {
	data               []*entity.User
	primaryKeyUsername map[string]*entity.User
	autoinctement      int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:               make([]*entity.User, 0, 10),
		primaryKeyUsername: make(map[string]*entity.User, 10),
		autoinctement:      1,
	}
}

func (u *UserStorage) Create(user entity.User) (entity.User, error) {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyUsername[user.Username]; ok {
		return entity.User{}, fmt.Errorf("user whith username %s  already exists\n", user.Username)
	}
	user.ID = u.autoinctement
	u.primaryKeyUsername[user.Username] = &user
	u.autoinctement++
	u.data = append(u.data, &user)
	return user, nil
}

func (u *UserStorage) Find(username string) (entity.User, error) {
	u.Lock()
	defer u.Unlock()
	if user, ok := u.primaryKeyUsername[username]; ok {
		return *user, nil
	} else {
		return entity.User{}, fmt.Errorf("not found user with username: %s", username)
	}
}

func (u *UserStorage) Delete(username string) error {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyUsername[username]; ok {
		delete(u.primaryKeyUsername, username)
		for i := range u.data {
			if u.data[i].Username == username {
				u.data = append(u.data[:i], u.data[i+1:]...)
				break
			}
		}
		return nil
	} else {
		return fmt.Errorf("not found user with username: %s", username)
	}
}

func (u *UserStorage) Update(username string, user entity.User) (entity.User, error) {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyUsername[username]; ok {
		delete(u.primaryKeyUsername, username)
		u.primaryKeyUsername[user.Username] = &user
		for i := range u.data {
			if username == u.data[i].Username {
				u.data[i] = &user
			}
		}
		return user, nil
	} else {
		return entity.User{}, fmt.Errorf("not found user with username: %s", user.Username)
	}
}
