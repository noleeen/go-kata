package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
)

var (
	pet1Test = entity.Pet{
		ID: 1,
		Category: entity.Category{
			ID:   0,
			Name: "string",
		},
		Name: "Buffe",
		PhotoUrls: []string{
			"1.jpg",
			"2.jpg",
		},
		Tags:   []entity.Category{},
		Status: "available",
	}

	pet2Test = entity.Pet{
		ID: 2,
		Category: entity.Category{
			ID:   0,
			Name: "string",
		},
		Name: "Joric",
		PhotoUrls: []string{
			"3.jpg",
			"4.jpg",
		},
		Tags:   []entity.Category{},
		Status: "available",
	}
)

func TestPetStorage_Create(t *testing.T) {
	rep := NewPetStorage()
	assert.Equal(t, pet1Test, rep.Create(pet1Test))
	assert.Equal(t, pet2Test, rep.Create(pet2Test))
}
func TestPetStorage_Update(t *testing.T) {
	rep := NewPetStorage()
	rep.Create(pet1Test)
	pet1Test.Name = "Leto"
	updatePet, err := rep.Update(pet1Test)
	assert.NoError(t, err)
	assert.Equal(t, pet1Test.Name, "Leto")
	assert.Equal(t, pet1Test, updatePet)
}

func TestPetStorage_Delete(t *testing.T) {
	rep := NewPetStorage()
	rep.Create(pet1Test)
	err := rep.Delete(5)
	assert.Error(t, err)
	assert.Equal(t, len(rep.data), 1)

	err = rep.Delete(1)
	assert.NoError(t, err)
	assert.Equal(t, len(rep.data), 0)
}

func TestPetStorage_GetById(t *testing.T) {
	rep := NewPetStorage()
	rep.Create(pet1Test)
	getPet, err := rep.GetById(1)
	assert.NoError(t, err)
	assert.Equal(t, pet1Test, getPet)

	getPet2, err := rep.GetById(2)
	assert.Error(t, err)
	assert.Equal(t, entity.Pet{}, getPet2)
}

func TestPetStorage_GetList(t *testing.T) {
	rep := NewPetStorage()
	assert.Equal(t, []entity.Pet{}, rep.GetList())
	rep.Create(pet1Test)
	rep.Create(pet2Test)
	tempMap := []entity.Pet{pet1Test, pet2Test}
	assert.Equal(t, tempMap, rep.GetList())
}

func TestPetStorage_UploadImage(t *testing.T) {
	rep := NewPetStorage()
	rep.Create(pet1Test)
	newImage := "5.jpg"
	uploadPet, err := rep.UploadImage(1, newImage)
	tempImages := append(pet1Test.PhotoUrls, newImage)
	assert.NoError(t, err)
	assert.Equal(t, tempImages, uploadPet.PhotoUrls)

	_, err = rep.UploadImage(2, newImage)
	assert.Error(t, err)
}
