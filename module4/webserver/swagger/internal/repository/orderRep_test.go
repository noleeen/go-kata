package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
)

var (
	order1Test = entity.Order{
		ID:       1,
		PetID:    0,
		Quantity: 0,
		ShipDate: "",
		Status:   "good order",
		Complete: false,
	}
	order2Test = entity.Order{
		ID:       2,
		PetID:    0,
		Quantity: 0,
		ShipDate: "",
		Status:   "good order",
		Complete: false,
	}
	order3Test = entity.Order{
		ID:       3,
		PetID:    0,
		Quantity: 0,
		ShipDate: "",
		Status:   "bad order",
		Complete: false,
	}
)

func TestOrderStorage_Create(t *testing.T) {
	rep := NewOrderStorage()
	assert.Equal(t, order1Test, rep.Create(order1Test))
	assert.Equal(t, order2Test, rep.Create(order2Test))
	assert.Equal(t, order3Test, rep.Create(order3Test))
	assert.Equal(t, 3, len(rep.data))
	assert.Equal(t, 2, rep.statuses["good order"])
}

func TestOrderStorage_Delete(t *testing.T) {
	rep := NewOrderStorage()
	rep.Create(order1Test)
	rep.Create(order2Test)
	assert.Equal(t, 2, len(rep.data))
	err := rep.Delete(2)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(rep.data))
	err = rep.Delete(2)
	assert.Error(t, err)

}

func TestOrderStorage_Find(t *testing.T) {
	rep := NewOrderStorage()
	rep.Create(order1Test)
	findOrder, err := rep.Find(1)
	assert.NoError(t, err)
	assert.Equal(t, order1Test, findOrder)
	_, err = rep.Find(2)
	assert.Error(t, err)

}

func TestOrderStorage_ListStatusCodes(t *testing.T) {
	rep := NewOrderStorage()
	rep.Create(order1Test)
	rep.Create(order2Test)
	rep.Create(order3Test)
	tempMap := make(map[string]int, 10)
	tempMap[order1Test.Status]++
	tempMap[order2Test.Status]++
	tempMap[order3Test.Status]++
	assert.Equal(t, tempMap, rep.ListStatusCodes())

}
