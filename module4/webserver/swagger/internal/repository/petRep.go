package repository

import (
	"fmt"
	"sync"

	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
)

type PetStorage struct {
	data               []*entity.Pet
	primaryKeyIDx      map[int]*entity.Pet
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:               make([]*entity.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*entity.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p *PetStorage) Create(pet entity.Pet) entity.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)
	return pet
}

func (p *PetStorage) Update(pet entity.Pet) (entity.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[pet.ID]; ok {
		for i := 0; i < len(p.data); i++ {
			if pet.ID == p.data[i].ID {
				p.data[i] = &pet
			}
		}
		p.primaryKeyIDx[pet.ID] = &pet
		return pet, nil
	} else {
		return entity.Pet{}, fmt.Errorf("not found pet with ID: %d\n", pet.ID)
	}
}

func (p *PetStorage) Delete(petId int) error {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[petId]; ok {
		delete(p.primaryKeyIDx, petId)
		for i := range p.data {
			if p.data[i].ID == petId {
				p.data = append(p.data[:i], p.data[i+1:]...)
				break
			}
		}

		return nil
	} else {
		return fmt.Errorf("not found ID: %d\n", petId)
	}
}

func (p *PetStorage) GetById(petId int) (entity.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if v, ok := p.primaryKeyIDx[petId]; ok { // если в мапе есть значение с таким ключом
		return *v, nil
	}
	return entity.Pet{}, fmt.Errorf("not found value with id: %d", petId)
}

func (p *PetStorage) GetList() []entity.Pet {
	p.Lock()
	defer p.Unlock()

	res := make([]entity.Pet, 0, len(p.data))
	for i := range p.data {
		res = append(res, *p.data[i])
	}
	return res
}

func (p *PetStorage) UploadImage(petID int, url string) (entity.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if pet, ok := p.primaryKeyIDx[petID]; ok {
		pet.PhotoUrls = append(pet.PhotoUrls, url)
		for i := 0; i < len(p.data); i++ {
			if pet.ID == p.data[i].ID {
				p.data[i] = pet
			}
		}
		return *pet, nil
	} else {
		return entity.Pet{}, fmt.Errorf("pet  with ID: %d not found", petID)
	}
}
