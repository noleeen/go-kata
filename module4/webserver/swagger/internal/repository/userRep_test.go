package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
)

var (
	user1Test = entity.User{
		ID:         1,
		Username:   "Kloun",
		FirstName:  "",
		LastName:   "",
		Email:      "",
		Password:   "",
		Phone:      "",
		UserStatus: 0,
	}
	user2Test = entity.User{
		ID:         2,
		Username:   "Circus",
		FirstName:  "",
		LastName:   "",
		Email:      "",
		Password:   "",
		Phone:      "",
		UserStatus: 0,
	}
)

func TestUserStorage_Create(t *testing.T) {
	rep := NewUserStorage()
	newUser, err := rep.Create(user1Test)
	assert.NoError(t, err)
	assert.Equal(t, user1Test, newUser)
	_, err = rep.Create(user1Test)
	assert.Error(t, err)
}

func TestUserStorage_Delete(t *testing.T) {
	rep := NewUserStorage()
	_, _ = rep.Create(user1Test)
	_, _ = rep.Create(user2Test)
	err := rep.Delete(user1Test.Username)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(rep.data))

	err = rep.Delete(user1Test.Username)
	assert.Error(t, err)
}

func TestUserStorage_Find(t *testing.T) {
	rep := NewUserStorage()
	_, _ = rep.Create(user1Test)
	findUser, err := rep.Find(user1Test.Username)
	assert.NoError(t, err)
	assert.Equal(t, user1Test, findUser)

	_, err = rep.Find("user1Test.Username")
	assert.Error(t, err)
}

func TestUserStorage_Update(t *testing.T) {
	rep := NewUserStorage()
	_, _ = rep.Create(user1Test)
	user1Test.Email = "1"
	user1Test.Username = "Shut"
	username := "Kloun"
	updateUser, err := rep.Update(username, user1Test)
	assert.NoError(t, err)
	assert.Equal(t, user1Test, updateUser)

	_, err = rep.Update(username, user1Test)
	assert.Error(t, err)
}
