package repository

import (
	"fmt"
	"sync"

	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
)

type OrderStorage struct {
	data          []*entity.Order
	primaryKeyIDx map[int]*entity.Order
	autoinctement int
	statuses      map[string]int
	sync.Mutex
}

func NewOrderStorage() *OrderStorage {
	return &OrderStorage{
		data:          make([]*entity.Order, 0, 10),
		primaryKeyIDx: make(map[int]*entity.Order, 10),
		statuses:      make(map[string]int, 5),
		autoinctement: 1,
	}
}

func (o *OrderStorage) Create(order entity.Order) entity.Order {
	o.Lock()
	defer o.Unlock()
	order.ID = o.autoinctement

	//if value, ok := o.statuses[order.Status]; ok {
	//	value++
	//} else {
	//	value = 1
	//}
	//возможно вот так сработает
	o.statuses[order.Status]++

	o.data = append(o.data, &order)
	o.primaryKeyIDx[order.ID] = &order
	o.autoinctement++

	return order
}

func (o *OrderStorage) Find(orderId int) (entity.Order, error) {
	o.Lock()
	defer o.Unlock()
	if order, ok := o.primaryKeyIDx[orderId]; ok {
		return *order, nil
	} else {
		return entity.Order{}, fmt.Errorf("not found value with ID: %d\n", orderId)
	}
}

func (o *OrderStorage) Delete(orderId int) error {
	o.Lock()
	defer o.Unlock()
	if _, ok := o.primaryKeyIDx[orderId]; ok {
		delete(o.primaryKeyIDx, orderId)
		for i := range o.data {
			if o.data[i].ID == orderId {
				o.data = append(o.data[:i], o.data[i+1:]...)
				break
			}
		}
		return nil
	} else {
		return fmt.Errorf("not found value with ID: %d\n", orderId)
	}
}

func (o *OrderStorage) ListStatusCodes() map[string]int {
	o.Lock()
	defer o.Unlock()
	return o.statuses
}
