package transport

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/service"
)

type handlersPet struct {
	service service.PetService
}

func GetHandlerPet() Handler {
	return &handlersPet{service: *service.NewPetService()}
}

func (p *handlersPet) Register(r chi.Router) {

	r.Post("/pet", p.PetCreate)
	r.Get("/pet/{petID}", p.PetGetByID)
	r.Put("/pet", p.PetUpdate)
	r.Delete("/pet/{petID}", p.PetDelete)
	r.Get("/pet", p.PetList)

	r.Post("/pet/{petID}/uploadImage", p.PetUploadImage)
	r.Post("/pet/{petID}", p.PetUpdateWithForm)

}

func (p *handlersPet) PetUploadImage(w http.ResponseWriter, r *http.Request) {
	petIdFromRequest, err := strconv.Atoi(chi.URLParam(r, "petID"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_ = r.ParseMultipartForm(32 << 20)
	file, m, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close() //nolint:all

	name := strconv.Itoa(petIdFromRequest) + ".*" + filepath.Ext(m.Filename)
	f, err := os.CreateTemp("module4/webserver/swagger/img/", name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer f.Close() //nolint:all
	_, _ = io.Copy(f, file)

	image, err := p.service.ServiceUploadImage(petIdFromRequest, "/img/"+filepath.Base(f.Name()))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляемв json и utf-8
	pet, err := json.Marshal(image)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = w.Write(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (p *handlersPet) PetUpdateWithForm(w http.ResponseWriter, r *http.Request) {
	petId, err := strconv.Atoi(chi.URLParam(r, "petID"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	pet, err := p.service.ServiceGetById(petId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	params := r.URL.Query()
	pet.Name = params["name"][0]
	pet.Status = params["status"][0]
	updatePet, _ := p.service.ServiceUpdate(pet)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	if err := json.NewEncoder(w).Encode(updatePet); err != nil { // записываем результат newPet json в http.ResponseWriter
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (p *handlersPet) PetCreate(w http.ResponseWriter, r *http.Request) {
	var newPet entity.Pet
	//можно с помощью Unmarshal пример в PetUpdate
	err := json.NewDecoder(r.Body).Decode(&newPet) // считываем приходящий json из запроса *http.Request в структуру newPet
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить работу нашего хендлера
	}
	newPet = p.service.ServiceCreate(newPet)                         // создаём запись в нашем хранилище
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляемв json и utf-8
	err = json.NewEncoder(w).Encode(newPet)                          // записываем результат newPet json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *handlersPet) PetUpdate(w http.ResponseWriter, r *http.Request) {
	var newPet entity.Pet
	readBody, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.Unmarshal(readBody, &newPet); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	updatePet, err := p.service.ServiceUpdate(newPet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляемв json и utf-8
	responsePet, err := json.Marshal(updatePet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = w.Write(responsePet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (p *handlersPet) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		newPet   entity.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	newPet, err = p.service.ServiceGetById(petID) // пытаемся получить Pet по id
	if err != nil {                               // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(newPet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *handlersPet) PetDelete(w http.ResponseWriter, r *http.Request) {
	var (
		err   error
		petID int
	)

	petID, err = strconv.Atoi(chi.URLParam(r, "petID"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := p.service.ServiceDelete(petID); err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}

	_, err = fmt.Fprintf(w, "pet with ID: %d deleted\n", petID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (p *handlersPet) PetList(w http.ResponseWriter, r *http.Request) {
	petList := p.service.ServiceGetList()
	petListBytes, err := json.MarshalIndent(petList, "", " ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	_, err = w.Write(petListBytes)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
