package transport

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"

	"github.com/go-chi/chi"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/service"
)

type handlerOrder struct {
	service service.OrderService
}

func (o *handlerOrder) Register(r chi.Router) {
	r.Post("/store/order", o.OrderCreate)
	r.Get("/store/{orderID}", o.OrderFind)
	r.Delete("/store/{orderID}", o.OrderDelete)
	r.Get("/store/inventory", o.OrderListStatusCodes)
}

func GetHandlerOrder() Handler {
	return &handlerOrder{service: *service.NewOrderService()}
}

func (o *handlerOrder) OrderCreate(w http.ResponseWriter, r *http.Request) {
	var newOrder entity.Order
	err := json.NewDecoder(r.Body).Decode(&newOrder)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}

	newOrder = o.service.ServiceOrderCreate(newOrder)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(newOrder); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}

}

func (o *handlerOrder) OrderFind(w http.ResponseWriter, r *http.Request) {
	orderIdRaw := chi.URLParam(r, "orderID")
	orderId, err := strconv.Atoi(orderIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
	}
	newOrder, err := o.service.ServiceOrderFind(orderId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(newOrder); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}
}

func (o *handlerOrder) OrderDelete(w http.ResponseWriter, r *http.Request) {
	orderIdRaw := chi.URLParam(r, "orderID")
	orderId, err := strconv.Atoi(orderIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
	}
	err = o.service.ServiceOrderDelete(orderId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}
	if err := json.NewEncoder(w).Encode(fmt.Sprintf("order with ID: %d deleted", orderId)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}

}

func (o *handlerOrder) OrderListStatusCodes(w http.ResponseWriter, r *http.Request) {
	if err := json.NewEncoder(w).Encode(o.service.ServiceOrderListStatusCodes()); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}
}
