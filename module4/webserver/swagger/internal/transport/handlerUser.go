package transport

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/service"
)

type handlerUser struct {
	service service.UserService
}

func GetHandlerUser() Handler {
	return &handlerUser{service: *service.NewUserService()}
}

func (u *handlerUser) Register(r chi.Router) {
	r.Post("/user", u.UserCreate)
	r.Get("/user/{username}", u.UserFind)
	r.Delete("/user/{username}", u.UserDelete)
	r.Put("/user/{username}", u.UserUpdate)
	r.Post("/user/createWithArray", u.UserCreateWithArray)
}

func (u *handlerUser) UserCreate(w http.ResponseWriter, r *http.Request) {
	var newUser entity.User
	if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
	}
	newUser, err := u.service.ServiceUserCreate(newUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(newUser); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}
}

func (u *handlerUser) UserFind(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "username")
	findUser, err := u.service.ServiceUserFind(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(findUser); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}

}

func (u *handlerUser) UserDelete(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "username")
	err := u.service.ServiceUserDelete(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}
	_, err = fmt.Fprintf(w, "user with username: %s deleted\n", username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}
}

func (u *handlerUser) UserUpdate(w http.ResponseWriter, r *http.Request) {
	var newUser entity.User
	if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}

	username := chi.URLParam(r, "username")

	newUser, err := u.service.ServiceUserUpdate(username, newUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(newUser); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}

}

func (u *handlerUser) UserCreateWithArray(w http.ResponseWriter, r *http.Request) {
	var (
		newUsers []entity.User
	)
	if err := json.NewDecoder(r.Body).Decode(&newUsers); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}
	for _, user := range newUsers {
		_, err := u.service.ServiceUserCreate(user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest) //400
			continue
		}
		_, err = fmt.Fprintf(w, "users with username: %s added\n", user.Username)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError) //500
			return
		}
	}

}
