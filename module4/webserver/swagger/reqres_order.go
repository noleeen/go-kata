package main

import "gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"

func init() {
	_ = orderAddRequest{}
	_ = orderAddResponse{}
	_ = orderDeleteRequest{}
	_ = orderDeleteResponse{}
	_ = orderGetByIdRequest{}
	_ = orderGetByIdResponse{}
	_ = orderListStatusCodesResponse{}

}

// swagger:route  POST /store/order store orderAddRequest
// Добавление заказа.
// responses:
// 200: orderAddResponse
// 400: description: Bad request
// 500: description: Internal server error

//swagger:parameters orderAddRequest
type orderAddRequest struct {
	//in:body
	Body entity.Order
}

//swagger:response orderAddResponse
type orderAddResponse struct {
	//in:body
	Body entity.Order
}

// swagger:route  DELETE /store/{id} store orderDeleteRequest
// Удаление заказа.
// responses:
// 200: orderDeleteResponse
// 400: description: Bad request
// 404: description: Not Found
// 500: description: Internal server error

//swagger:parameters orderDeleteRequest
type orderDeleteRequest struct {
	//ID of order
	//
	//In:path
	ID string `json:"id"`
}

//swagger:response orderDeleteResponse
type orderDeleteResponse struct {
	//in:body
	Body string
}

// swagger:route  GET /store/{id} store orderGetByIdRequest
// Поиск заказа по id.
// responses:
// 200: orderGetByIdResponse
// 400: description: Bad request
// 404: description: Not Found
// 500: description: Internal server error

//swagger:parameters orderGetByIdRequest
type orderGetByIdRequest struct {
	//ID of order
	//
	//In:path
	ID string `json:"id"`
}

//swagger:response orderGetByIdResponse
type orderGetByIdResponse struct {
	//in:body
	Body entity.Order
}

// swagger:route  GET /store/inventory store orderListStatusCodesRequest
// Список статусов.
// responses:
// 200: orderListStatusCodesResponse
// 500: description: Internal server error

//swagger:response orderListStatusCodesResponse
type orderListStatusCodesResponse struct {
	//in:body
	Statuses map[string]int
}
