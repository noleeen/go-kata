package main

import (
	"bytes"

	"gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"
)

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
// 200: petAddResponse

// swagger:parameters petAddRequest
type petAddRequest struct { //nolint: all
	// in:body
	Body entity.Pet
}

//swagger:response petAddResponse
type petAddResponse struct { //nolint:all
}

// swagger:route GET /pet/{id} pet petGetByIDRequest
// ПОлучение питомца по ID.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct { //nolint: all
	// ID of an item
	//
	// In: path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct { //nolint: all
	// in:body
	Body entity.Pet
}

// swagger:route GET /pet pet petListRequest
// ПОлучение списка питомцев.
// responses:
//   200: petListResponse

// swagger:response petListResponse
type petListResponse struct { //nolint: all
	// in:body
	Body []entity.Pet
}

// swagger:route PUT /pet pet petUpdate
// Обновление питомца.
// responses:
// 200: petUpdateResponse

// swagger:parameters petUpdate
type petUpdateRequest struct { //nolint: all
	//in:body
	Body entity.Pet
}

// swagger:response petUpdateResponse
type petUpdateResponse struct { //nolint: all
	// in:body
	Body entity.Pet
}

// swagger:route DELETE /pet/{id} pet petDelete
// Удаление питомца.
// responses:
// 200: petDelete

// swagger:parameters petDelete
type petDeleteRequest struct { //nolint: all
	// ID of pet
	//
	// In: path
	ID string `json:"id"`
}

// swagger:route POST /pet/{id}/uploadImage pet petUploadImageRequest
// Добавление изображения по ID.
// responses:
//  200: petUploadImageResponse
//	400: description: bad request

// swagger:parameters petUploadImageRequest
type petUploadImageRequest struct { //nolint:all
	// ID of pet
	// required: true
	// in:path
	ID int64 `json:"id"`

	// file to upload
	// in: formData
	// swagger:file
	File *bytes.Buffer `json:"file"`
}

//swagger:response petUploadImageResponse
type petUploadImageResponse struct { //nolint:all
	Body entity.Pet
}

// swagger:route POST /pet/{petID} pet petUpdateWithFormRequest
// Обновление данных питомца по ID используя форму.
// responses:
//  200: petUpdateWithFormResponse

// swagger:parameters petUpdateWithFormRequest
type petUpdateWithFormRequest struct { //nolint:all
	// ID of pet
	// required: true
	// in:path
	ID int64 `json:"petID"`

	// new name
	// in: query
	Name string `json:"name"`

	// new status
	// in: query
	Status string `json:"status"`
}

//swagger:response petUpdateWithFormResponse
type petUpdateWithFormResponse struct { //nolint:all
	// in:body
	Body entity.Pet
}
