package main

import "gitlab.com/go-kata/module4/webserver/swagger/internal/domain/entity"

func init() {
	_ = userAddRequest{}
	_ = userAddResponse{}
	_ = userDeleteRequest{}
	_ = userDeleteResponse{}
	_ = userGetByUsernameRequest{}
	_ = userGetByUsernameResponse{}
	_ = userUserUpdateRequest{}
	_ = userUserUpdateResponse{}
	_ = userAddWithArrayRequest{}
	_ = userAddWithArrayResponse{}
}

// swagger:route  POST /user user userAddRequest
// Добавление пользователя.
// responses:
// 200: userAddResponse
// 400: description: Bad request
// 500: description: Internal server error

// swagger:parameters userAddRequest
type userAddRequest struct {
	//in:body
	Body entity.User
}

// swagger:response userAddResponse
type userAddResponse struct {
	//in:body
	Body entity.User
}

// swagger:route  DELETE /user/{username} user userDeleteRequest
// Удаление пользователя.
// responses:
// 200: userDeleteResponse
// 400: description: Bad request
// 404: description: Not Found
// 500: description: Internal server error

// swagger:parameters userDeleteRequest
type userDeleteRequest struct {
	//Username of user
	//
	//In:path
	Username string `json:"username"`
}

// swagger:response userDeleteResponse
type userDeleteResponse struct {
	//in:body
	Body string
}

// swagger:route  GET /user/{username} user userGetByUsernameRequest
// Получение пользователя по username.
// responses:
// 200: userGetByUsernameResponse
// 400: description: Bad request
// 404: description: Not Found
// 500: description: Internal server error

// swagger:parameters userGetByUsernameRequest
type userGetByUsernameRequest struct {
	//Username of user
	//
	//In:path
	Username string `json:"username"`
}

// swagger:response userGetByUsernameResponse
type userGetByUsernameResponse struct {
	//in:body
	Body entity.User
}

// swagger:route  PUT /user/{username} user userUserUpdateRequest
// Обновить пользователя по username.
// responses:
// 200: userUserUpdateResponse
// 400: description: Bad request
// 404: description: Not Found
// 500: description: Internal server error

// swagger:parameters userUserUpdateRequest
type userUserUpdateRequest struct {
	//Username of user
	//
	//In:path
	Username string `json:"username"`

	//in:body
	Body entity.User
}

// swagger:response userUserUpdateResponse
type userUserUpdateResponse struct {
	//in:body
	Body entity.User
}

// swagger:route  POST /user/createWithArray user userAddWithArrayRequest
// Добавление пользователей с помощью списка.
// responses:
// 200: userAddWithArrayResponse
// 400: description: Bad request
// 500: description: Internal server error

// swagger:parameters userAddWithArrayRequest
type userAddWithArrayRequest struct {
	//in:body
	Body []entity.User
}

// swagger:response userAddWithArrayResponse
type userAddWithArrayResponse struct {
	//in:body
	Body string
}
