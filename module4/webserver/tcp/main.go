package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

func handleConnection(conn net.Conn) {
	defer conn.Close() //nolint: all
	reader := bufio.NewReader(conn)

	// read the request
	request, _ := reader.ReadString('\n')
	fmt.Println(request)

	// parse the request
	parts := strings.Split(request, " ")
	method := parts[0]
	path := parts[1]

	//write the response
	switch {
	case method == "GET" && path == "/":
		m := "HTTP/1.1 200 OK\n"
		m = m + "Server: nginx/1.19.2\n"
		m = m + "Date: Mon, 19 Oct 2020 13:13:29 GMT\n"
		m = m + "Content-Type: text/html\n"
		m = m + "Content-Length: 98\n" // указываем размер контента
		m = m + "Last-Modified: Mon, 19 Oct 2020 13:13:13 GMT\n"
		m = m + "Connection: keep-alive\n"
		m = m + "ETag: \"5f8d90e9-62\"\n"
		m = m + "Accept-Ranges: bytes\n"
		m = m + "\n"
		m = m + "<!DOCTYPE html>\n"
		m = m + "<html>\n"
		m = m + "<head>\n"
		m = m + "<title>Webserver</title>\n"
		m = m + "</head>\n"
		m = m + "<body>\n"
		m = m + "Yuo can Do It!\n"
		m = m + "</body>\n"
		m = m + "</html>\n"
		conn.Write([]byte(m)) //nolint: all
	case method == "POST" && path == "/upload":
		// read the file ftom the request body
		file, _ := os.Create("uploaded_file.txt")
		defer file.Close()                                                      //nolint: all
		io.Copy(file, reader)                                                   //nolint: all
		fmt.Fprintf(conn, "HTTP/1.1 200 OK\r\n\r\nFile uploaded successfully!") //nolint: all
	default:
		_, err := fmt.Fprintf(conn, "HTTP/1.1 404 Not Found\r\n\r\n")
		if err != nil {
			return
		}

	}

}

func main() {
	// слушаем порт 8080
	listener, _ := net.Listen("tcp", ":8080")
	defer func(listener net.Listener) {
		if err := listener.Close(); err != nil {
			log.Fatal(err)
		}
	}(listener)

	for {
		//принимаем входящее соединение
		conn, _ := listener.Accept()
		go handleConnection(conn)

	}

}
