package parser

import "gitlab.com/go-kata/module4/selenium/internal/model"

type VacancyParser interface {
	ParseStart() ([]model.Vacancy, error)
}
