package parser

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"gitlab.com/go-kata/module4/selenium/internal/model"
)

const maxTries = 7

type parserVacancy struct {
	driver selenium.WebDriver
	query  string
}

func NewParserVacancy() VacancyParser {
	return &parserVacancy{}
}

func (p *parserVacancy) ParseStart() ([]model.Vacancy, error) {
	//прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome", //"browserName": "firefox"
	}

	//добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)
	p.query = "golang"

	var err error

	//прописываем адрес нашего драйвера
	urlPrefix := selenium.DefaultURLPrefix

	//немного костылей чтоб драйвер не падал
	i := 1
	for i < maxTries {
		p.driver, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	//после окончания программы завершаем работу по запросу
	defer p.driver.Quit() //nolint:all

	count, err := p.GetCountVacancies()
	if err != nil {
		return nil, err
	}
	links, err := p.GetLinksOfPages(count)
	if err != nil {
		return nil, err
	}

	vacancies := make([]model.Vacancy, 0, len(links))

	for _, link := range links {
		v, err := p.GetVacancy(link)
		if err != nil {
			continue
		}
		vacancies = append(vacancies, v)
	}
	return vacancies, nil
}

func (p *parserVacancy) GetCountVacancies() (int, error) {
	//сразу обращаемся к странице с поиском вакансий по запросу
	page := 1 //номер страницы

	err := p.driver.Get(fmt.Sprintf("https://career.habr.com/vacncies?page=%d&q=%s&type=all", page, p.query))
	if err != nil {
		return 0, err
	}

	elem, err := p.driver.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, err
	}
	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}
	fmt.Println("vacancyCountRaw:", vacancyCountRaw)
	count, err := strconv.Atoi(vacancyCountRaw)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (p *parserVacancy) GetLinksOfPages(count int) ([]string, error) {

	const HabrCareerLink = "https://career.habr.com"

	vacanciesOnPage := 25

	linksOfPages := make([]string, 0, count/vacanciesOnPage)

	for i := 1; i <= count/vacanciesOnPage; i++ {
		err := p.driver.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", i, p.query))
		if err != nil {
			return nil, err
		}

		elems, err := p.driver.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			return nil, err
		}

		for i := range elems {
			var link string
			link, err = elems[i].GetAttribute("href")
			if err != nil {
				continue
			}
			linksOfPages = append(linksOfPages, HabrCareerLink+link)
		}
	}
	return linksOfPages, nil
}

func (p *parserVacancy) GetVacancy(link string) (model.Vacancy, error) {

	resp, err := http.Get(link)
	if err != nil {
		return model.Vacancy{}, err
	}
	var doc *goquery.Document
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil && doc != nil {
		return model.Vacancy{}, err
	}
	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		log.Println("habr vacancy nodes not found")
		return model.Vacancy{}, nil
	}
	ss := dd.First().Text()

	var vacancy model.Vacancy
	if err := json.Unmarshal([]byte(ss), &vacancy); err != nil {
		return model.Vacancy{}, nil
	}
	return vacancy, nil
}
