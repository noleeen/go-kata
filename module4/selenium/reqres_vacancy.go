package main

import "gitlab.com/go-kata/module4/selenium/internal/model"

func init() {
	_ = NewVacancyParseResponse{}
	_ = VacancyGetByIdRequest{}
	_ = VacancyGetByIdResponse{}
	_ = VacanciesGetResponse{}
	_ = VacanciesDeleteRequest{}
	_ = VacanciesDeleteResponse{}
}

// swagger:route  POST /parse vacancy NewVacancyParseRequest
// Запись вакансий с сайта в репозиторий.
// responses:
// 200: description: NewVacancyParseResponse
// 500: description: Internal server error

//swagger:response NewVacancyParseResponse
type NewVacancyParseResponse struct {
	//in:body
	Body string
}

// swagger:route  GET /vacancy/{id} vacancy VacancyGetByIdRequest
// Поиск вакансии по id.
// responses:
// 200: VacancyGetByIdResponse
// 400: description: Bad request
// 404: description: Not Found
// 500: description: Internal server error

//swagger:parameters VacancyGetByIdRequest
type VacancyGetByIdRequest struct {
	//ID of order
	//
	//In:path
	ID string `json:"id"`
}

//swagger:response VacancyGetByIdResponse
type VacancyGetByIdResponse struct {
	//in:body
	Body model.Vacancy
}

// swagger:route  GET /vacancy vacancy VacanciesGetRequest
// Показать все вакансии.
// responses:
// 200: VacanciesGetResponse
// 404: description: Not Found
// 500: description: Internal server error

//swagger:response VacanciesGetResponse
type VacanciesGetResponse struct {
	//in:body
	Vacancies []model.Vacancy
}

// swagger:route  DELETE /vacancy/{id} vacancy VacanciesDeleteRequest
// Удаление вакансии.
// responses:
// 200: VacanciesDeleteResponse
// 400: description: Bad request
// 404: description: Not Found
// 500: description: Internal server error

//swagger:parameters VacanciesDeleteRequest
type VacanciesDeleteRequest struct {
	//ID of order
	//
	//In:path
	ID string `json:"id"`
}

//swagger:response VacanciesDeleteResponse
type VacanciesDeleteResponse struct {
	//in:body
	Body string
}
