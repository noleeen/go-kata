package service

import (
	"gitlab.com/go-kata/module4/selenium/internal/model"
	"gitlab.com/go-kata/module4/selenium/internal/repository"
)

type vacancyService struct {
	rep repository.VacancyRepository
}

func NewVacancyService() VacancyService {
	return &vacancyService{rep: repository.NewVacancyRep()}
}

func (v *vacancyService) ServiceVacancyCreate(dto model.Vacancy) error {
	return v.rep.Create(dto)
}

func (v *vacancyService) ServiceVacancyGetById(id int) (model.Vacancy, error) {
	return v.rep.GetById(id)
}

func (v *vacancyService) ServiceVacancyGetList() ([]model.Vacancy, error) {
	return v.rep.GetList()
}

func (v *vacancyService) ServiceVacancyDelete(id int) error {
	return v.rep.Delete(id)
}
