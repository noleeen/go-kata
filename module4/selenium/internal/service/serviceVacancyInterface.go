package service

import "gitlab.com/go-kata/module4/selenium/internal/model"

type VacancyService interface {
	ServiceVacancyCreate(dto model.Vacancy) error
	ServiceVacancyGetById(id int) (model.Vacancy, error)
	ServiceVacancyGetList() ([]model.Vacancy, error)
	ServiceVacancyDelete(id int) error
}
