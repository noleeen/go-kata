package model

type Vacancy struct {
	Identifier     `json:"identifier" bson:"identifier"`
	Title          string `json:"title" db:"title" bson:"title"`
	Description    string `json:"description" db:"description" bson:"description"`
	DatePosted     string `json:"datePosted" db:"date_posted" bson:"date_posted"`
	ValidThrough   string `json:"validThrough" db:"valid_through" bson:"valid_through"`
	EmploymentType string `json:"employmentType" db:"employment_type" bson:"employment_type"`
}

type Identifier struct {
	Value string `json:"value" db:"vacancy_id" bson:"vacancy_id"`
	Name  string `json:"name" db:"organization" bson:"organization"`
}
