package repository

import "gitlab.com/go-kata/module4/selenium/internal/model"

type VacancyRepository interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}
