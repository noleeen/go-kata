package repository

import (
	"errors"
	"fmt"
	"strconv"
	"sync"

	"gitlab.com/go-kata/module4/selenium/internal/model"
)

type vacancyRep struct {
	data map[int]*model.Vacancy
	sync.Mutex
}

func NewVacancyRep() VacancyRepository {
	return &vacancyRep{data: make(map[int]*model.Vacancy, 100)}
}

func (v *vacancyRep) Create(dto model.Vacancy) error {
	v.Lock()
	defer v.Unlock()
	vacancyId, err := strconv.Atoi(dto.Identifier.Value)
	if err != nil {
		return err
	}
	v.data[vacancyId] = &dto
	return nil
}

func (v *vacancyRep) GetById(id int) (model.Vacancy, error) {
	v.Lock()
	defer v.Unlock()
	if v, ok := v.data[id]; ok {
		return *v, nil
	} else {
		return model.Vacancy{}, fmt.Errorf("vacancy with id: %d not found", id)
	}
}

func (v *vacancyRep) GetList() ([]model.Vacancy, error) {
	v.Lock()
	defer v.Unlock()

	if len(v.data) == 0 {
		return nil, errors.New("there are no vacancies")
	}

	vacancies := make([]model.Vacancy, len(v.data))
	for _, vacancy := range v.data {
		vacancies = append(vacancies, *vacancy)
	}
	return vacancies, nil
}

func (v *vacancyRep) Delete(id int) error {
	v.Lock()
	defer v.Unlock()

	if _, ok := v.data[id]; ok {
		delete(v.data, id)
		return nil
	} else {
		return fmt.Errorf("vacancy with id: %d not found", id)
	}
}
