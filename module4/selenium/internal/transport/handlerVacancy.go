package transport

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/go-kata/module4/selenium/pkg/parser"

	"github.com/go-chi/chi"
	"gitlab.com/go-kata/module4/selenium/internal/service"
)

type handlerVacancy struct {
	service service.VacancyService
}

func GetHandlerVacancy() Handlers {
	return &handlerVacancy{service: service.NewVacancyService()}
}

func (h *handlerVacancy) Register2(r chi.Router) {
	r.Post("/parse", h.NewVacancyParse)
	r.Get("/vacancy/{vacancyId}", h.VacancyGetById)
	r.Get("/vacancy", h.VacanciesGet)
	r.Delete("/vacancy/{vacancyId}", h.VacanciesDelete)
}

func (h *handlerVacancy) NewVacancyParse(w http.ResponseWriter, r *http.Request) {
	newParser := parser.NewParserVacancy()
	vacancies, err := newParser.ParseStart()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}
	for _, vacancy := range vacancies {
		if err := h.service.ServiceVacancyCreate(vacancy); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	_, err = fmt.Fprint(w, "the bird in the cage")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}
}

func (h *handlerVacancy) VacancyGetById(w http.ResponseWriter, r *http.Request) {
	vacancyIdRaw := chi.URLParam(r, "vacancyId")
	vacancyId, err := strconv.Atoi(vacancyIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}
	vacancy, err := h.service.ServiceVacancyGetById(vacancyId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(vacancy); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
	}
}

func (h *handlerVacancy) VacanciesGet(w http.ResponseWriter, r *http.Request) {
	vacancies, err := h.service.ServiceVacancyGetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode(vacancies); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
	}

}

func (h *handlerVacancy) VacanciesDelete(w http.ResponseWriter, r *http.Request) {
	vacancyIdRaw := chi.URLParam(r, "vacancyId")
	vacancyId, err := strconv.Atoi(vacancyIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //400
		return
	}
	err = h.service.ServiceVacancyDelete(vacancyId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound) //404
		return
	}

	_, err = fmt.Fprintf(w, "vacancy with id: %d deleted\n", vacancyId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError) //500
		return
	}

}
