package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/go-kata/module4/selenium/internal/transport"
)

func main() {
	port := ":8080"
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	vacancyHandler := transport.GetHandlerVacancy()
	swaggerHandler := transport.GetHandlerSwagger()

	vacancyHandler.Register2(r)
	swaggerHandler.Register2(r)

	transport.RunServer(r, port)
}
