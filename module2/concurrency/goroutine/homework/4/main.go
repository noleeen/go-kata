package main

import (
	"fmt"
	"time"
)

func main() {
	msg1 := make(chan string)
	msg2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			msg1 <- "прошло пол секунды"
		}
	}()
	go func() {
		for {
			time.Sleep(time.Second * 2)
			msg2 <- "прошло 2 секунды"
		}
	}()

	for {
		select {
		case msg := <-msg1:
			fmt.Println(msg)
		case msg := <-msg2:
			fmt.Println(msg)
		}
	}
}
