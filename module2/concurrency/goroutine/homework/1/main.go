package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	t := time.Now()
	rand.Seed(t.UnixNano())
	go parseUrl("http://example.com/")
	parseUrl("http://youtube.com/")

	fmt.Printf("Parsing completed. Time Elapesed: %.2f secomds\n", time.Since(t).Seconds())

}

func parseUrl(url string) {
	for i := 0; i < 5; i++ {
		latency := rand.Intn(500) + 500                       //случайное число от  500 до 1000
		time.Sleep(time.Duration(latency) * time.Millisecond) //  возвращает количество миллисекунд (от 0,5сек до 1 сек)
		fmt.Printf("Parsing <%s> - Step %d - Latency %d\n", url, i+1, latency)
	}
}
