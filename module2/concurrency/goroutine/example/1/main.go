package main

import (
	"log"
	"math/rand"
	"sync"
	"time"
)

var wg sync.WaitGroup

func SayGreetings(greeting string, times int) {
	for i := 0; i < times; i++ {
		log.Println(greeting)
		d := time.Second * time.Duration(rand.Intn(5)) / 2
		time.Sleep(d) // sleep for 0 to 2,5 seconds
	}
	//notify a task is finished
	wg.Done() // <=> wg.Add(-1)
}

func main() {
	rand.Seed(time.Now().UnixNano())
	log.SetFlags(0)
	wg.Add(2)
	go SayGreetings("hi!", 10)
	go SayGreetings("hellow!", 10)
	//time.Sleep(2 * time.Second) // основная горутина ждёт 2 секунды перед завершением
	wg.Wait() // ждёт пока все задачи не завершатся

}
