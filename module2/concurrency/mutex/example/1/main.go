package main

import (
	"fmt"
	"sync"
)

var counter int = 0

func main() {
	var mutex sync.Mutex
	ch := make(chan bool)
	for i := 0; i < 5; i++ {
		go work(i, ch, &mutex)
	}
	//ожидаем завершения всех горутин
	for i := 1; i < 5; i++ {
		<-ch
	}
	fmt.Println("the end")
}

func work(num int, ch chan bool, mutex *sync.Mutex) {
	mutex.Lock()
	counter = 0
	for k := 1; k <= 5; k++ {
		counter++
		fmt.Println("Goroutine", num, "-", counter)
	}
	mutex.Unlock()
	ch <- true
}
