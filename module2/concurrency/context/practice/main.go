package main

import (
	"fmt"
	"time"
)

func main() {
	processLongTask()
}

func processLongTask() {
	time.Sleep(2 * time.Second)
	fmt.Println("done processing")
}
