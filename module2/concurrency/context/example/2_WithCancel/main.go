//Представьте, что вы очень торопитесь на важную встречу. Чтобы добраться побыстрее, вы решаете вызвать такси.
//Где быстрее найдётся машина, вы не знаете, поэтому решаете начать поиск в нескольких сервисах одновременно.
//Когда в одном из них найдётся машина, вы отмените поиск в остальных.
//Для похожих и других задач можно использовать контекст с функцией отмены.
//
//Давайте представим, как описанная ситуация могла бы выглядеть в виде кода:
package main

import (
	"context"
	"log"
	"math/rand"
	"sync"
	"time"
)

func main() {
	var (
		resCh       = make(chan string)
		ctx, cancel = context.WithCancel(context.Background())
		services    = []string{"f_serv", "go taxi", "fast poni", "turtle"}
		winner      string
		wg          sync.WaitGroup
	)
	defer cancel()

	for i := range services {
		srv := services[i]
		wg.Add(1)
		go func() {
			requestRide(ctx, resCh, srv)
			wg.Done()
		}()
	}

	go func() {
		winner = <-resCh
		cancel()
	}()
	wg.Wait()
	log.Printf("found car in %q", winner)
}

func requestRide(ctx context.Context, resCh chan string, servName string) {
	time.Sleep(3 * time.Second)
	for {
		select {
		case <-ctx.Done():
			log.Printf("stopped the search in %q (%v)", servName, ctx.Err())
			return
		default:
			if rand.Float64() < 0.5 {
				resCh <- servName
				return
			}
			continue
		}
	}
}
