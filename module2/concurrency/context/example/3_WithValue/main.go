//// Когда стоит передавать данные через контекст?
////Короткий ответ — никогда. Передача данных через контекст является антипаттерном, поскольку это порождает неявный контракт между компонентами вашего приложения, к тому же ещё и ненадёжный. Исключение составляют случаи, когда вам нужно предоставить компоненту из внешней библиотеку вашу реализацию интерфейса, который вы не можете менять. Например, middleware в HTTP сервере.
////
////Пример. HTTP Middleware
////Представьте, что вы хотите, чтобы ваш API принимал запросы только от аутентифицированных клиентов. Однако вызывать методы для аутентификации в каждом обработчике не кажется удачной идеей. Но вы можете сделать так, чтобы перед тем как вызовется обработчик запроса, вызвался метод, который проведёт аутентификацию, и либо вызовет следующий метод в цепочке (в данном случае обработчик), либо вернёт HTTP с ошибкой аутентификации. Это и есть пример классического middleware.
////
//// https://blog.ildarkarymov.ru/posts/context-guide/
//
////Вот как это может выглядеть:
//
package main

//
//import (
//	"context"
//	"fmt"
//	"io"
//	"log"
//	"net/http"
//)
//
//type ctxKey string
//
//const keyUserID ctxKey = "user_id"
//
//func main() {
//	mux := http.NewServeMux()
//
//	mux.Handle("/restricted", authMiddleware(handleRestricted()))
//
//	if err := http.ListenAndServe(":3000", mux); err != nil {
//		log.Fatal(err)
//	}
//}
//
//func authMiddleware(next http.Handler) http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		token := r.Header.Get("Token")
//
//		if token != "very-secret-token" {
//			w.WriteHeader(http.StatusUnauthorized)
//			io.WriteString(w, "invalid token")
//			return
//		}
//
//		ctx := context.WithValue(r.Context(), keyUserID, 42)
//		next.ServeHTTP(w, r.WithContext(ctx))
//	})
//}
//
//func handleRestricted() http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		userID, ok := r.Context().Value(keyUserID).(int)
//		if !ok {
//			w.WriteHeader(http.StatusInternalServerError)
//			io.WriteString(w, "internal error, try again later please")
//			return
//		}
//
//		io.WriteString(w, fmt.Sprintf("hello, user #%d!", userID))
//	})
//}
