package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var closeTicker = time.NewTicker(30 * time.Second)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}
		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}
		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	c := make(chan int, 1000)
	go func() {
		defer close(c)
		for {
			select {
			case _, open := <-c:
				if !open {
					return
				}
			case c <- rand.Intn(100):
			case <-closeTicker.C:
				return
			}
		}
	}()
	return c
}

func main() {
	rand.Seed(time.Now().UnixNano())
	startTime := time.Now()
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()
	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()
	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()
	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	mainC := joinChannels(a, b, c)

	for val := range mainC {
		fmt.Println(val)
	}
	fmt.Println(time.Since(startTime))

}
