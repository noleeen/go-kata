package main

import "fmt"

//Существует два способа использования буферизованных каналов с одной емкостью в качестве мьютексов:
//
//Блокировка через отправку, разблокировка через получение.
//
//Блокировка через получение, разблокировка через отправку.
//
//Ниже приведен пример блокировки сквозной отправки.

func main() {
	//the capacity must be one
	mutex := make(chan struct{}, 1)

	counter := 0
	increase := func() {
		mutex <- struct{}{} // lock
		counter++
		<-mutex // unlock
	}

	increase1000 := func(done chan<- struct{}) {
		for i := 0; i < 1000; i++ {
			increase()
		}
		done <- struct{}{}
	}

	done := make(chan struct{})
	go increase1000(done)
	go increase1000(done)
	<-done
	<-done
	fmt.Println(counter)
}

//---------------------------------------------------
// пример блокировки через получение.
//func main () {
//	mutex := make(chan struct{},1)
//	mutex <- struct{}{} //this line is needed
//
//	counter := 0
//	inc := func() {
//		<-mutex // lock
//		counter++
//		mutex <- struct{}{} // unlock
//	}
//}
