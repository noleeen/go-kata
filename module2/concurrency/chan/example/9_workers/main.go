package main

import (
	"fmt"
	"sync"
	"time"
)

func sqr(g *sync.WaitGroup, r <-chan int, w chan<- int, id int) {
	for num := range r {
		time.Sleep(time.Second / 5)
		fmt.Printf("[worker %d] result %d\n", id, num)
		w <- num * num
	}
	g.Done()
}

func main() {
	wg := sync.WaitGroup{}
	read := make(chan int, 10)
	write := make(chan int, 10)

	for i := 1; i <= 3; i++ {
		wg.Add(1)
		go sqr(&wg, read, write, i)
	}
	for i := 0; i < 5; i++ {
		read <- i * 2
	}
	close(read)

	wg.Wait()

	for i := 0; i < 4; i++ {
		fmt.Printf("main %d result %d\n", i, <-write)
	}
	fmt.Println("finish")

}
