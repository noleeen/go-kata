package main

import (
	"fmt"
	"math/rand"
	"os"
	"sort"
)

func main() {
	val := make([]byte, 32*1024*1024)
	if _, err := rand.Read(val); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	done := make(chan struct{})

	go func() {
		sort.Slice(val, func(i, j int) bool {
			return val[i] < val[j]
		})
		done <- struct{}{}
	}()

	<-done
	fmt.Println(val[0], val[len(val)-1])
	//fmt.Println(val)
}
