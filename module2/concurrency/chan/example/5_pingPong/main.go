package main

// Две горутины могут вести диалог через канал. Ниже приведен пример, который напечатает серию чисел Фибоначчи.

import (
	"fmt"
	"os"
	"time"
)

type Ball uint64

func Play(name string, table chan Ball) {
	var lastValue Ball = 1
	for {
		ball := <-table // get the ball
		fmt.Println(name, ball)
		ball += lastValue
		if ball < lastValue { // overflow
			os.Exit(0)
		}
		lastValue = ball
		table <- ball //bat back the ball
		time.Sleep(time.Second)

	}
}

func main() {
	table := make(chan Ball)
	go func() {
		table <- 1 // throw ball on the table
	}()
	go Play("A:", table)
	Play("B:", table)
}
