package main

import (
	"log"
	"math/rand"
	"time"
)

type Seat int
type Bar chan Seat

//В приведенной выше оптимизированной версии будет сосуществовать не более десяти активных клиентских горутин,
//но все еще будет много клиентских горутин, созданных за время существования программы.

//func (bar Bar) ServeCustomer(c int, seat Seat) {
//	log.Print("++ customer#", c, " drinks at seat#", seat)
//	time.Sleep(time.Second * time.Duration(2+rand.Intn(6)))
//	log.Print("--customer#", c, " frees seat#", seat)
//	bar <- seat //free seat and leave the bar
//}
//func main() {
//	rand.Seed(time.Now().UnixNano())
//
//	//the bar has 10 seats
//	bar24x7 := make(Bar, 10)
//	//place seats in a bar
//	for seatId := 0; seatId < cap(bar24x7); seatId++ {
//		//none of the sends will block
//		bar24x7 <- Seat(seatId)
//	}
//
//	for customerID := 0; ; customerID++ {
//		time.Sleep(time.Second)
//		//need a seat to serve next customer
//		seat := <-bar24x7
//		go bar24x7.ServeCustomer(customerID, seat)
//	}
//
//	//sleeping != blocking
//	for {
//		time.Sleep(time.Second) //nolint
//	}
//}

//-----------------------------------------------------------------------
//В более эффективной реализации, показанной ниже, за время существования программы будет создано
//не более десяти горутин, обслуживающих клиентов.

func (bar Bar) ServeCustomer(consumers chan int) {
	for c := range consumers {
		seatID := <-bar
		log.Print("++ customer#", c, " drinks at seat#", seatID)
		time.Sleep(time.Second * time.Duration(2+rand.Intn(6)))
		log.Print("--customer#", c, " frees seat#", seatID)
		bar <- seatID //free seat and leave the bar
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())

	//the bar has 10 seats
	bar24x7 := make(Bar, 10)
	//place seats in a bar
	for seatId := 0; seatId < cap(bar24x7); seatId++ {
		//none of the sends will block
		bar24x7 <- Seat(seatId)
	}

	consumers := make(chan int)
	for i := 0; i < cap(bar24x7); i++ {
		go bar24x7.ServeCustomer(consumers)
	}

	for customerID := 0; ; customerID++ {
		time.Sleep(time.Second)
		//need a seat to serve next customer
		consumers <- customerID
	}

}
