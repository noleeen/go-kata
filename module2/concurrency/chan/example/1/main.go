//package main
//
//import (
//	"fmt"
//	"math/rand"
//	"time"
//)
//
//func LongTimeRequest() <-chan int32 {
//	r := make(chan int32)
//
//	go func() {
//		time.Sleep(3 * time.Second)
//		ran := rand.Int31n(100)
//		r <- ran
//	}()
//	return r
//}
//
//func SumSquers(a, b int32) int32 {
//	return a*a + b*b
//}
//
//func main() {
//	rand.Seed(time.Now().UnixNano())
//
//	a, b := LongTimeRequest(), LongTimeRequest()
//	fmt.Println(<-a, <-b)
//
//}

//---------------------------------------------------------------------------------------------------------------------
// Как и в предыдущем примере, в следующем примере значения двух аргументов вызова функции sumSquares запрашиваются
//одновременно. В отличие от последнего примера, функция longTimeRequest принимает в качестве параметра канал только
//для отправки, а не возвращает результат канала только для приема.
//
//package main
//
//import (
//	"fmt"
//	"math/rand"
//	"time"
//)
//
//func LongTimeRequest(r chan<- int32) {
//	//simulate a workload
//	time.Sleep(3 * time.Second)
//	r <- rand.Int31n(100)
//}
//
//func SumSquers(a, b int32) int32 {
//	return a*a + b*b
//}
//
//func main() {
//	rand.Seed(time.Now().UnixNano())
//
//	//	ra, rb := make(chan int32), make(chan int32)
//	//	go LongTimeRequest(ra)
//	//	go LongTimeRequest(rb)
//	//	fmt.Println(<-ra, <-rb)
//	//На самом деле, в указанном выше примере нам не нужны два канала для передачи результатов.
//	//Использование одного канала — это нормально.
//	ra := make(chan int32, 2)
//	go LongTimeRequest(ra)
//	go LongTimeRequest(ra)
//	fmt.Println(<-ra, <-ra)
//
//}

//------------------------------------------------------------------------------------------------------------------
//Быстрый ответ
//
//Это усовершенствование варианта с использованием только одного канала в предыдущем примере.
//
//Иногда часть данных может быть получена из нескольких источников, чтобы избежать больших задержек. Для многих факторов продолжительность отклика этих источников может сильно различаться. Даже для указанного источника длительность его отклика также непостоянна. Чтобы сделать продолжительность ответа как можно короче, мы можем отправить запрос каждому источнику в отдельной горутине. Будет использован только первый ответ, остальные более медленные будут отброшены.
//
//Обрати внимание: если есть N источников, пропускная способность канала связи должна быть не менее N-1, чтобы навсегда избежать блокировки горутин, соответствующих отброшенным ответам.

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func source(c chan<- int32) {
	randNum, randSec := rand.Int31n(100), rand.Intn(3)+1
	time.Sleep(time.Duration(randSec) * time.Second)
	c <- randNum
}

func main() {
	rand.Seed(time.Now().UnixNano())

	startTime := time.Now()
	// c must be a buffered channel
	c := make(chan int32, 2)
	for i := 0; i < cap(c); i++ {
		go source(c)
	}
	r := <-c
	t := <-c
	t1 := <-c
	fmt.Println(time.Since(startTime))
	fmt.Println(r, t, t1)
}
