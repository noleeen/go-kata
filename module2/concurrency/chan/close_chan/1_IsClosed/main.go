package main

import "fmt"

type T int

func isClosed(ch <-chan T) bool {
	select {
	case <-ch:
		return true
	default:
	}
	return false
}

func main() {
	c := make(chan T, 2)
	var val T = 2

	fmt.Println(SafeSend(c, val))

	fmt.Println(isClosed(c))
	fmt.Println(SafeClose(c)) // тут канал закрылся

	//close(c)
	fmt.Println(SafeClose(c))
	fmt.Println(isClosed(c))
}

//Если ты в любом случае закроешь канал со стороны получателя или одного из нескольких отправителей канала,
//ты можете использовать механизм восстановления, чтобы предотвратить возможную панику из-за сбоя программы.

func SafeClose(ch chan T) (justClose bool) {
	defer func() {
		if recover() != nil {
			justClose = false
		}
	}()
	close(ch)
	return true
	//если канал уже был закрыт, то будет паника,
	//которую перехватит рековер и сказел нам false - канал  закрыть не удалось(уже был закрыт),
	//в противном случае канал закроется и ф-ия вернёт true
}

//Та же идея может быть использована для отправки значений в потенциально закрытый канал.
func SafeSend(ch chan T, value T) (closed bool) {
	defer func() {
		if recover() != nil {
			closed = true
		}
	}()

	ch <- value  // panic if ch is closed
	return false // <=> closed = false; return
}
