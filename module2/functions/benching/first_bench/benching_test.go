package first_bench

import "testing"

// insertXIntMap применяется для добавления Х элементов в Map[int]int
func insertXIntMap(x int, b *testing.B) {
	testmap := make(map[int]int, 0)
	//сбрасываем тайемр после инициализации мар
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// вставляем значение i в ключ i
		testmap[i] = i
	}

}

func insertXIntMapSizeFix(x int, b *testing.B) {
	testmap := make(map[int]int, x)
	//сбрасываем тайемр после инициализации мар
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// вставляем значение i в ключ i
		testmap[i] = i
	}

}

//func insertXIntMapFind(x int, b *testing.B) {
//	testmap := make(map[int]int, x)
//	//сбрасываем тайемр после инициализации мар
//	for i := 0; i < x; i++ {
//		// вставляем значение i в ключ i
//		testmap[i] = i
//	}
//	var holder int
//	b.ResetTimer()
//	for i := 0; i < x; i++ {
//		holder = testmap[i]
//	}
//	if holder != 0 {
//
//	}
//
//}

func BenchmarkFirst52(b *testing.B) {
	for i := 0; i < b.N; i++ {

		insertXIntMap(10000, b)
	}
}
func BenchmarkFirst51(b *testing.B) {
	for i := 0; i < b.N; i++ {

		insertXIntMapSizeFix(10000, b)
	}
}

//func BenchmarkFirst61(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//
//		insertXIntMapFind(10000, b)
//	}
//}

//func BenchmarkFirst59(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//
//		InsertXIntSlice(10000, b)
//	}
//}
//func BenchmarkFirst58(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//
//		InsertXIntSliceFixSize(10000, b)
//	}
//}
//func BenchmarkFirst68(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//
//		InsertXIntSliceFind(10000, b)
//	}
//}
