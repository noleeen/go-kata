package main

import "testing"

var u = genUsers()
var p = genProducts()
var mP = convertToMapProducts(p)

func BenchmarkMapUserProducts(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(u, p)
	}
}

func BenchmarkMapUserProducts2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(u, mP)
	}
}
