package main

import (
	"fmt"
	"math/rand"

	"github.com/brianvoe/gofakeit/v6"
)

type User struct {
	ID       int64
	Name     string `fake:"{firstname}"`
	Products []Product
}

type Product struct {
	UserId int64
	Name   string `fake:"{sentence:3}"`
}

func main() {
	products := genProducts() // создали слайс продуктов для ф-ии MapUserProducts

	users := genUsers() // создали слайс пользователей для ф-ии MapUserProducts

	// создаём новый слайс с пользователями для ф-ии MapUserProducts2  и копируем туда users
	//чтоб для двух наших функций были одинаковые входные данные для обработки
	users2 := make([]User, len(users))
	copy(users2, users)

	users = MapUserProducts(users, products) // добавляем продукты к пользователем с помощью слайса продуктов

	// далее преобразуем слайс продуктоа в карту для ф-ии MapUserProducts2
	//(можно было бы сразу сгенерировать карту продуктов, но тогда данные для ф-ии MapUserProducts и MapUserProducts2
	// отличались бы друг от друга из-за случайной генерации и нарушилась бы чистота эксперимента)
	mapProducts := convertToMapProducts(products)
	users2 = MapUserProducts2(users, mapProducts)

	// проверяем одинаково ли работают ф-ии MapUserProducts и MapUserProducts2
	fmt.Println(users[1])
	fmt.Println(users2[1])
}

func MapUserProducts(users []User, products []Product) []User {
	for i, user := range users {
		for _, product := range products {
			if product.UserId == user.ID {
				users[i].Products = append(users[i].Products, product)
			}
		}
	}
	return users
}
func MapUserProducts2(users []User, products map[int64][]Product) []User {

	for i := range users {
		users[i].Products = append(users[i].Products, products[int64(i)]...)
	}
	return users
}

//func MapUserProducts2(users []User, products []Product, b *testing.B) []User {
//	productsMap := make(map[int64][]Product, len(users))
//	for i, prod := range products {
//		productsMap[products[i].UserId] = append(productsMap[products[i].UserId], prod)
//	}
//	b.ResetTimer()
//	for i := range users {
//		users[i].Products = append(users[i].Products, productsMap[int64(i)]...)
//	}
//	return users
//}

func genProducts() []Product {
	products := make([]Product, 1000)
	for i, product := range products {
		_ = gofakeit.Struct(&product)
		product.UserId = int64(rand.Intn(100) + 1)
		products[i] = product
	}
	return products
}
func convertToMapProducts(products []Product) map[int64][]Product {
	//products := make([]Product, 1000)
	//for i, product := range products {
	//	_ = gofakeit.Struct(&product)
	//	product.UserId = int64(rand.Intn(100) + 1)
	//	products[i] = product
	//}

	productsMap := make(map[int64][]Product, 101)
	for i, prod := range products {
		productsMap[products[i].UserId] = append(productsMap[products[i].UserId], prod)
	}

	return productsMap
}

func genUsers() []User {
	users := make([]User, 100)
	for i, user := range users {
		_ = gofakeit.Struct(&user)
		user.ID = int64(i)
		users[i] = user
	}
	return users
}
