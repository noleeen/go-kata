package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func main() {
	//res, err := ExecuteMergeDictsJob(&MergeDictsJob{})
	//res, err := ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}})
	res, err := ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}})
	fmt.Printf("%+v, \"%v\" \n", res, err)

	//если один в один с заданием, то следующий блок больше подходит, но 25 строка мне больше понравилась)))
	//if err == errNotEnoughDicts {
	//	fmt.Printf("&MergeDictsJob{IsFinished: %t}, \"%v\"\n", res.IsFinished, err)
	//
	//} else if err == errNilDict {
	//	fmt.Printf("&MergeDictsJob{IsFinished: %t, Dicts: %#v}, \"%v\"\n", res.IsFinished, res.Dicts, err)
	//} else if err == nil {
	//	fmt.Printf("&MergeDictsJob{IsFinished: %t, Dicts: []%#v}, \"%v\"\n", res.IsFinished, res.Merged, err)
	//}
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	job.IsFinished = true

	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}
	for i := range job.Dicts {
		if job.Dicts[i] == nil {
			return job, errNilDict
		}
	}
	job.Merged = make(map[string]string)
	for i := range job.Dicts {
		for k, v := range job.Dicts[i] {
			job.Merged[k] = v
		}
	}
	return job, nil
}
