package main

import (
	"fmt"
	"unicode/utf8"
)

func Greet(name string) string {
	var c, l int
	for i := 0; i < len(name); {
		r, size := utf8.DecodeRuneInString(name[i:])
		if (r >= 'A' && r <= 'Z') || (r >= 'a' && r <= 'z') {
			l++
		} else if (r >= 'А' && r <= 'Я') || (r >= 'а' && r <= 'я') {
			c++
		}
		i += size
	}

	if l > 0 && c == 0 {
		return fmt.Sprintf("Hello %s, you welcome!", name)
	} else if c > 0 && l == 0 {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	} else {
		return "input error"
	}

}
