package main

import "testing"

func TestGreet(t *testing.T) {
	type args struct {
		name string
	}

	tests := []struct {
		name   string
		args   args
		expect string
	}{
		{"english name",
			args{"Sally"},
			"Hello Sally, you welcome!"},
		{"russian name",
			args{"Казимир"},
			"Привет Казимир, добро пожаловать!"},
		{"error name",
			args{"Казsdfмир"},
			"input error"},
	}
	for _, el := range tests {
		t.Run(el.name, func(t *testing.T) {
			if got := Greet(el.args.name); got != el.expect {
				t.Errorf("Greet() = %s, want %s\n", got, el.expect)
			}
		})
	}
}
