package main

import (
	"errors"
	"fmt"
	"math"
)

//калькулятор с округлением до 3 знаков после запятой

type calc struct {
	a, b, result float64
}

func newCalc() *calc {
	return &calc{}
}

func (c *calc) SetA(a float64) *calc {
	c.a = a
	return c
}

func (c *calc) SetB(b float64) *calc {
	c.b = b
	return c
}

func (c *calc) Do(operation func(a, b float64) float64) *calc {
	c.result = operation(c.a, c.b)
	return c
}

func (c *calc) Result() float64 {
	return c.result
}

func multiply(a, b float64) float64 {
	return math.Round((a*b)*1000) / 1000
}

func divide(a, b float64) float64 {
	res, err := divideZero(a, b)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	return res
}

func divideZero(a, b float64) (float64, error) {
	if b == 0 {
		return 0, errors.New("Error divide: you can't divide by zero")
	}
	return math.Round((a/b)*1000) / 1000, nil
}

func sum(a, b float64) float64 {
	return math.Round((a+b)*1000) / 1000
}

func average(a, b float64) float64 {
	return math.Round((a+b)/2*1000) / 1000
}

func main() {
	calc1 := newCalc()

	resDiv := calc1.SetA(10).SetB(0).Do(divide).Result()
	resMult := calc1.SetA(10).SetB(2).Do(multiply).Result()
	resSum := calc1.SetA(10).SetB(2).Do(sum).Result()
	resAver := calc1.SetA(10).SetB(2).Do(average).Result()
	fmt.Println(resDiv)
	fmt.Println(resMult)
	fmt.Println(resSum)
	fmt.Println(resAver)
	fmt.Println(calc1.result)
	res2 := calc1.Result()
	fmt.Println(res2 == resAver)
}
