package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMultiply(t *testing.T) {
	fmt.Println("\n______start TestMultiply_________")
	type args struct {
		a, b float64
	}
	type testCase struct {
		name   string
		args   args
		expect float64
	}

	testValues := []testCase{
		{
			name:   "a == b ",
			args:   args{a: 7, b: 7},
			expect: 49,
		},
		{
			name:   "a > b ",
			args:   args{a: 7, b: 2},
			expect: 14,
		},
		{
			name:   "a < b ",
			args:   args{a: 7, b: 10},
			expect: 70,
		},
		{
			name:   "a or b < 0 ",
			args:   args{a: -7, b: 7},
			expect: -49,
		},
		{
			name:   "a and b < 0 ",
			args:   args{a: -7, b: -7},
			expect: 49,
		},
		{
			name:   "a or b == 0 ",
			args:   args{a: -7, b: 0},
			expect: 0,
		},
		{
			name:   "a or b == float ",
			args:   args{a: -7, b: 5.4},
			expect: -37.8,
		},
		{
			name:   "a and b == float ",
			args:   args{a: 5.5, b: 2.2},
			expect: 12.1,
		},
	}

	for _, el := range testValues {
		t.Run(el.name, func(t *testing.T) {
			assert.Equalf(t, el.expect, multiply(el.args.a, el.args.b), "multiply(%v, %v)", el.args.a, el.args.b)
		})
	}
}

func TestDivide(t *testing.T) {
	fmt.Println("\n______start TestDivide_________")
	type testCase struct {
		a, b, res float64
	}
	testValues := []testCase{
		{10, 10, 1},
		{10, 5, 2},
		{5, 10, 0.5},
		{5, -10, -0.5},
		{-50, -10, 5},
		{-50, 0, 0},
		{0, 2, 0},
		{-50.4, 2, -25.2},
	}

	for _, el := range testValues {
		expect := el.res
		recive := divide(el.a, el.b)
		assert.Equal(t, expect, recive)
		fmt.Printf("case a=%v, b=%v expect to %v, recive resalt = %v\n", el.a, el.b, expect, recive)
	}
}

func TestSum(t *testing.T) {
	fmt.Println("\n______start TestSum_________")
	type testCase struct {
		a, b, res float64
	}
	testValues := []testCase{
		{10, 10, 20},
		{10, 5, 15},
		{5, 10.5, 15.5},
		{5, -10, -5},
		{-50, -10, -60},
		{-50, 0, -50},
		{0, 2, 2},
		{-50.4, 2.2, -48.2},
	}

	for _, el := range testValues {
		expect := el.res
		recive := sum(el.a, el.b)
		assert.Equalf(t, expect, recive, "sum(a=%v and b=%v) expect to %v, recieve to %v", el.a, el.b, expect, recive)
	}
}

func TestAverage(t *testing.T) {
	fmt.Println("\n______start TestAverage_________")
	type testCase struct {
		a, b, res float64
	}
	testValues := []testCase{
		{10, 10, 10},
		{10, 5, 7.5},
		{5, 10.5, 7.75},
		{5, -10, -2.5},
		{-50, -10, -30},
		{-50, 0, -25},
		{0, 2, 1},
		{-50.4, 2.2, -24.1},
	}

	for _, el := range testValues {
		expect := el.res
		recive := average(el.a, el.b)
		assert.Equalf(t, expect, recive, "average(a=%v and b=%v) expect to %v, recieve to %v", el.a, el.b, expect, recive)
	}
}
