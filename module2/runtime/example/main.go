package main

import (
	"fmt"
	"runtime"
	"strconv"
	"time"
)

func printMemStats(message string, rtm runtime.MemStats) {
	fmt.Println("\n===", message, "===")
	fmt.Println("Mallocs:", rtm.Mallocs) // показывает сколько раз программа выделила память во время выполнения
	fmt.Println("Frees:", rtm.Frees)     // frees - освобождает
	fmt.Println("LiveObject:", rtm.Mallocs-rtm.Frees)
	fmt.Println("PauseTotalNs:", rtm.PauseTotalNs)
	fmt.Println("NumGC:", rtm.NumGC)
	fmt.Println("NumGC:", time.UnixMilli(int64(rtm.LastGC/1_000_000)))
	fmt.Println("HeapObjects:", rtm.HeapObjects) // сколько объектов было выделено в куче во время выполнения
	fmt.Println("HeapAlloc:", rtm.HeapAlloc)     // сколько памяти было выделено в куче в данный момент
}

func main() {
	var rtm runtime.MemStats
	runtime.ReadMemStats(&rtm)
	printMemStats("Start", rtm)

	a := make([]int64, 0)
	var i int64
	for i = 0; i < 10_000_000; i++ {
		a = append(a, i) //nolint: all
	}

	runtime.ReadMemStats(&rtm)
	printMemStats("After 10 000 000 int64 appends", rtm)
	var b []string

	//var j int
	for j := 0; j < 10_000_000; j++ {
		b = append(b, strconv.Itoa(j)) //nolint: all
	}
	runtime.ReadMemStats(&rtm)
	printMemStats("After 10 000 000 int appends", rtm)

	runtime.GC()
	runtime.ReadMemStats(&rtm)
	printMemStats("After forced GC", rtm)

}
