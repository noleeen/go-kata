package main

import "testing"

var (
	pets Pet
	err  error
	data []byte
)

func BenchmarkStandartUnmarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPet(DataJ)
		if err != nil {
			panic(err)
		}
		//_ = pets
	}
}
func BenchmarkStandartMarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
		//_ = data
	}
}

func BenchmarkJsonIterUnmarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPet2(DataJ)
		if err != nil {
			panic(err)
		}
	}
}
func BenchmarkJsonIter(b *testing.B) {
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
	}
}
