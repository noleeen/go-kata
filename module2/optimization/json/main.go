package main

import (
	"encoding/json"

	jsoniter "github.com/json-iterator/go"
)

type Pet []PetElement
type PetElement struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    Status     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type Status string

const (
	Pending Status = "pending"
)

func UnmarshalPet(data []byte) (Pet, error) {
	var r Pet
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pet) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalPet2(data []byte) (Pet, error) {
	var r Pet
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Pet) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

var DataJ []byte = []byte(`[
  {
    "id": 9223372036854775000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "tiger",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854775000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "tiger",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854775000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "tiger",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854775000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "tiger",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854775000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "tiger",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 243981,
    "category": {
      "id": 801001,
      "name": "dog"
    },
    "name": "Elwanek",
    "photoUrls": [],
    "tags": [
      {
        "id": 655325,
        "name": "for kids"
      },
      {
        "id": 954295,
        "name": "small"
      }
    ],
    "status": "pending"
  },
  {
    "id": 410211,
    "category": {
      "id": 743178,
      "name": ""
    },
    "name": "Elwanek",
    "photoUrls": [
      "/Users/a/Documents/Sel/workspace/PetStoreAPITesting1 "
    ],
    "tags": [],
    "status": "pending"
  },
  {
    "id": 235547,
    "category": {
      "id": 527561,
      "name": ""
    },
    "name": "Maja",
    "photoUrls": [],
    "tags": [],
    "status": "pending"
  },
  {
    "id": 856258,
    "category": {
      "id": 1,
      "name": "сats"
    },
    "name": "Lucifer",
    "photoUrls": [
      "https://prm.ua/wp-content/uploads/2019/08/64470184_625413447965191_804540562614301408_n.jpg"
    ],
    "tags": [
      {
        "id": 0,
        "name": "friendly"
      },
      {
        "id": 1,
        "name": "funny"
      }
    ],
    "status": "pending"
  },
  {
    "id": 1234567,
    "category": {
      "id": 1,
      "name": "art"
    },
    "name": "alanci",
    "photoUrls": [
      "photo"
    ],
    "tags": [
      {
        "id": 1,
        "name": "sato"
      },
      {
        "id": 2,
        "name": "mato"
      }
    ],
    "status": "pending"
  },
  {
    "id": 1805,
    "category": {
      "id": 1805,
      "name": "cat"
    },
    "name": "murzik",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 1804,
        "name": "shepherd"
      }
    ],
    "status": "pending"
  },
  {
    "id": 8675309,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "GoldenRetriever",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 177929,
    "category": {
      "id": 138166,
      "name": "dog"
    },
    "name": "Elwanek",
    "photoUrls": [],
    "tags": [
      {
        "id": 151295,
        "name": "for kids"
      },
      {
        "id": 786963,
        "name": "small"
      }
    ],
    "status": "pending"
  },
  {
    "id": 977273,
    "category": {
      "id": 189093,
      "name": ""
    },
    "name": "Elwanek",
    "photoUrls": [
      "/Users/a/Documents/Sel/workspace/PetStoreAPITesting1 "
    ],
    "tags": [],
    "status": "pending"
  },
  {
    "id": 880545,
    "category": {
      "id": 205413,
      "name": ""
    },
    "name": "Maja",
    "photoUrls": [],
    "tags": [],
    "status": "pending"
  },
  {
    "id": 146749,
    "category": {
      "id": 1,
      "name": "Angora"
    },
    "name": "Delbert",
    "photoUrls": [
      "<string>",
      "<string>"
    ],
    "tags": [
      {
        "id": 12,
        "name": "mew"
      }
    ],
    "status": "pending"
  },
  {
    "id": 37193747,
    "category": {
      "id": 41053860,
      "name": "adipisicing cillum culpa consequat aliqua"
    },
    "name": "doggie",
    "photoUrls": [
      "deserunt magna sit ullamco consequat",
      "ex pariatur commodo reprehenderit in"
    ],
    "tags": [
      {
        "id": 46959469,
        "name": "aliquip in reprehenderit fugiat"
      },
      {
        "id": -15442745,
        "name": "id d"
      }
    ],
    "status": "pending"
  },
  {
    "id": 6739257,
    "category": {
      "id": 10,
      "name": "string"
    },
    "name": "Changed Kitty Price",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 10,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 190190965,
    "category": {
      "id": 10,
      "name": "string"
    },
    "name": "Changed Kitty Price",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 10,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 4321,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "parrot_pending",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 7722551,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "cat",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 753757,
    "category": {
      "id": 88,
      "name": "kate"
    },
    "name": "cat",
    "photoUrls": [
      "<string>",
      "<string>"
    ],
    "tags": [
      {
        "id": 0,
        "name": "mew"
      }
    ],
    "status": "pending"
  },
  {
    "id": 98545316515555,
    "category": {
      "id": 8,
      "name": "pchel"
    },
    "name": "juju",
    "photoUrls": [
      "da",
      "net"
    ],
    "tags": [
      {
        "id": 55,
        "name": "pchelo"
      },
      {
        "id": 999985746215,
        "name": "pchelo"
      }
    ],
    "status": "pending"
  },
  {
    "id": 90210,
    "category": {
      "id": 7,
      "name": "Ferret"
    },
    "name": "Franz",
    "photoUrls": [],
    "tags": [],
    "status": "pending"
  },
  {
    "id": 31662904,
    "category": {
      "id": -72976378,
      "name": "consequat commodo dolor"
    },
    "name": "doggie",
    "photoUrls": [
      "dolor",
      "aliquip"
    ],
    "tags": [
      {
        "id": -81536039,
        "name": "officia non exercitation laborum"
      },
      {
        "id": -81700539,
        "name": "sed quis"
      }
    ],
    "status": "pending"
  }
]`)
