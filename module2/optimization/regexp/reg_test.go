package main

import "testing"

func BenchmarkSanitizeText(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for i := range data {
			SanitizeText(data[i])
		}
	}
}

func BenchmarkFilterText_SanitizeTextOptimize(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeTextOptimize(data[i])
		}
	}
}
