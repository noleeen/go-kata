package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeFloat()
}

func typeFloat() {
	fmt.Println("=== Start type FLOAT ===")

	var uintNum uint32 = 1 << 29 // сдвиг на 29 ячеек в 30 позицию
	uintNum += 1 << 28           // добавляем 1 в 29 позицию
	uintNum += 1 << 27           // добавляем 1 в 28 позицию
	uintNum += 1 << 26           // добавляем 1 в 27 позицию
	uintNum += 1 << 25           // добавляем 1 в 26 позицию
	// добавим мантиссу
	uintNum += 1 << 21 // добавляем 1 в 22 позицию
	uintNum += 1 << 31 // выставляем 32 бит в 1. для отображения знака минус
	var floatNum = *(*float32)(unsafe.Pointer(&uintNum))
	fmt.Println(floatNum)

	a, b := 2.3329, 3.1234
	c := a + b
	fmt.Println("mistake1: ", c)

	a = 9.99999
	b2 := float64(a)
	fmt.Println("mistake2: ", b2)

	a4 := 5.2
	b4 := 4.1
	fmt.Println(a4 + b4)
	fmt.Println((a4 + b4) == 9.3) //will return True

	c4 := 5.2
	d4 := 2.1
	fmt.Println(c4 + d4)
	fmt.Println((c4 + d4) == 7.3) //will return False

	fmt.Println("=== END type float ===")
}
