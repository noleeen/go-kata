package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is ", unsafe.Sizeof(n), "bytes")
	var numUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8: ", numUint8)

	numUint8 = (1 << 8) - 1
	fmt.Println("uint8 max value: ", numUint8)
}
