package main

import "fmt"

func main() {
	var numInt int = 3
	var numFloat float32 = float32(numInt)
	fmt.Printf("type: %T, value: %v\n", numFloat, numFloat)
	numInt = int(numFloat)
	fmt.Printf("type: %T, value: %v\n", numInt, numInt)

}
