package main

import (
	"fmt"
	"reflect"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Location struct {
	Adress string
	City   string
	Index  string
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	wallet := Wallet{250000, 3500, 1, 4}
	user := User{
		Age:    13,
		Name:   "Alex",
		Wallet: wallet,
	}

	user2 := User{
		Age:      34,
		Name:     "Anton",
		Wallet:   Wallet{144000, 8900, 55, 34},
		Location: Location{"Olesheva 3-288", "Minsk", "22090"},
	}

	user3 := [4]User{
		{
			Age:      65,
			Name:     "Anton",
			Wallet:   Wallet{144000, 8900, 55, 34},
			Location: Location{"Olesheva 3-288", "Minsk", "22090"},
		},
		{
			Age:      15,
			Name:     "Olga",
			Wallet:   Wallet{14000, 800, 5, 4},
			Location: Location{"Olesheva 3-288", "Minsk", "22090"},
		},
		{
			Age:      18,
			Name:     "Alesya",
			Wallet:   Wallet{180000, 1800, 5, 4},
			Location: Location{"Olesheva 3-288", "Minsk", "22090"},
		},
	}

	user.Wallet = wallet
	fmt.Println(user)
	fmt.Println(wallet)
	fmt.Println(unsafe.Sizeof(user))
	fmt.Println(unsafe.Sizeof(user2))
	fmt.Println(reflect.TypeOf(user))
	fmt.Println(user2)
	fmt.Println(user3)

	for i := range user3 {
		if user3[i].Age >= 18 {
			fmt.Println(user3[i])
		}
	}

	for i := range user3 {
		if user3[i].Wallet.USD > 1000 {
			fmt.Println(user3[i])
		}
	}

}
