package main

import (
	"fmt"
	"reflect"
)

type Man struct{}

func (m Man) Sleep() {}
func (m Man) Eat()   {}
func (m Man) Work()  {}

type Programmer interface {
	Work()
}

func main() {
	Vasiliy := Man{}
	fmt.Println(reflect.TypeOf(Vasiliy))

	var worker Programmer
	worker = Vasiliy

	fmt.Println(reflect.TypeOf(worker))
	//worker.Sleep()  НЕ РАБОТАЕТ !!!
	m, ok := worker.(Man)
	m.Sleep() // РАБОТАЕТ!))))
	fmt.Println(ok)

}
