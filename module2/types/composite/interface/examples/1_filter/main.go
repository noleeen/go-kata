package main

import "fmt"

type Filter interface {
	About() string
	Process([]int) []int
}

type UniqueFilter struct{}

func (UniqueFilter) About() string {
	return "remove duplicate numbers"
}

func (UniqueFilter) Process(input []int) []int {
	outs := make([]int, 0, len(input))
	pusheds := make(map[int]bool)
	for _, n := range input {
		if !pusheds[n] {
			pusheds[n] = true
			outs = append(outs, n)
		}
	}
	return outs
}

type MultipleFilter int

func (mf MultipleFilter) About() string {
	return fmt.Sprintf("keep multiples of %v", mf)
}
func (mf MultipleFilter) Process(input []int) []int {
	var outs = make([]int, 0, len(input))
	for _, n := range input {
		if n%int(mf) == 0 {
			outs = append(outs, n)
		}
	}
	return outs
}

func filterAndPrint(fltr Filter, unfiltered []int) []int {
	filtered := fltr.Process(unfiltered)
	fmt.Println(fltr.About()+":\n", filtered)
	return filtered
}

func main() {
	num := []int{12, 7, 21, 12, 12, 26, 25, 21, 30}
	fmt.Println(num)

	filters := []Filter{
		UniqueFilter{},
		MultipleFilter(2),
		MultipleFilter(3),
	}

	for _, fltr := range filters {
		num = filterAndPrint(fltr, num)
	}
}
