package main

import "fmt"

func main() {
	var x interface{} = 123

	//вариант 1
	n, ok := x.(int)
	fmt.Println(n, ok) // 123  true
	n = x.(int)
	fmt.Println(n) // 123

	//вариант 2
	a, ok := x.(float64)
	fmt.Println(a, ok) // 0 false
	//a = x.(float64)  will Panic ! ! !

	// ======================= TYPE_SWITCH =========================

	values := []interface{}{
		456, "asd", true, 0.13, int32(789),
		[]int{1, 2, 3}, map[int]bool{}, nil,
	}
	fmt.Println("========== TYPE_SWITCH ==========")
	for _, x := range values {
		switch v := x.(type) {
		case []int:
			fmt.Println("int slice:", v)
		case string:
			fmt.Println("string:", v)
		case int, int32, float64:
			fmt.Println("number:", v)
		case nil:
			fmt.Println(v)
		default:
			fmt.Println("others:", v)
		}
	}

}
