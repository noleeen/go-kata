package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)

	test(n)
}

func test(r interface{}) {
	switch t := r.(type) {
	case nil:
		if t == nil {
			fmt.Println("value = nil, type = nil")
		}
	case *int:
		if t == nil {
			fmt.Println("Success!")
		}
	default:
		fmt.Printf("unknown type is %T\n value is %v", r, r)
	}

}
