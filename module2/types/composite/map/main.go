// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type Project struct {
	Name  string
	Stars int
}

func main() {
	m := make(map[string]int)
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "я",
			Stars: 27601,
		},
		{
			Name:  "не ",
			Stars: 27602,
		},
		{
			Name:  "понял",
			Stars: 27603,
		},
		{
			Name:  "какие",
			Stars: 27604,
		},
		{
			Name:  "данные",
			Stars: 27605,
		},
		{
			Name:  "должны",
			Stars: 27606,
		},
		{
			Name:  "быть",
			Stars: 27607,
		},
		{
			Name:  "в",
			Stars: 27608,
		},
		{
			Name:  "этих",
			Stars: 27609,
		},
		{
			Name:  "полях",
			Stars: 27610,
		},
		{
			Name:  "и откуда",
			Stars: 27611,
		},
		{
			Name:  "их нужно было брать",
			Stars: 27612,
		},
		// сюда впишите ваши остальные 12 структур
	}

	// в цикле запишите в map
	for i := range projects {
		m[projects[i].Name] = projects[i].Stars
	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, value := range m {
		fmt.Println(value)

	}
}
