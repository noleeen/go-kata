package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	var x2, x1 User
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	for i := 0; i < len(users); i++ {
		if users[i].Age > 40 {
			users = append(users[:i], users[i+1:]...)
			i--
		}
	}
	fmt.Println(users)

	// 3. Удали первого и последнего пользователя, используя shift и pop из Slice tricks.

	x1, users = users[len(users)-1], users[:len(users)-1]
	x2, users = users[0], users[1:]

	fmt.Println("delete: ", x1, "and", x2)
	fmt.Println(users)
}
