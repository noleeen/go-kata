package main

import "fmt"

// 1 способ
//func main() {
//	s := []int{1, 2, 3}
//	s = Append(s)
//	fmt.Println(s)
//}
//
//func Append(s []int) []int {
//	s = append(s, 4)
//	return s
//}

//2 способ
func main() {
	s := []int{1, 2, 3}
	Append(&s)
	fmt.Println(s)
}

func Append(s *[]int) {
	*s = append(*s, 4)
}
