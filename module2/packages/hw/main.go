package main

import (
	"fmt"

	nerv "gitlab.com/noleeen/nervousbreakdown"
)

func main() {
	myOct := nerv.Octopus{Name: "Zigmund", Color: "brown"}
	nerv.Hello()
	fmt.Println(myOct.Str())

}
