package main

import (
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func appendToFile() {
	f, err := os.OpenFile("t2.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	if _, err = f.WriteString("and i'm ukraian's son"); err != nil {
		panic(err)
	}
}

func main() {
	data, err := os.ReadFile("module2/stl/files/t.txt")
	check(err)
	fmt.Println(string(data))

	err2 := os.WriteFile("t2.txt", data, 0777)
	check(err2)
	appendToFile()
}

//func readFile(filePath string)(string, error) {
//	data, err := os.ReadFile(filePath)
//	return string(data), err
//
//}
