package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

//https://gobyexample.com.ru/writing-files  тут с пояснениями
func main() {
	d1 := []byte("you good man")
	err := os.WriteFile("t.txt", d1, 0644)
	check(err)

	//запись с открытием файла для более детальной работы
	file, err := os.Create("newT.txt") //вместо открытия файла можно использовать create, и потом так же закрываем с пом. defer
	check(err)
	defer func(file *os.File) {
		err := file.Close()
		check(err)
	}(file)

	d2 := []byte{115, 111, 101, 10}
	n2, err := file.Write(d2)
	check(err)
	fmt.Printf("wrote %d bytes\n", n2)

	d3 := "sunRise under my home" // можно записать строку
	n3, err := file.WriteString(d3)
	check(err)
	fmt.Printf("wrote %d bytes\n", n3)

	err = file.Sync() //выполнить синхронизацию для сброса записей в отдельное хранилище
	check(err)

	w := bufio.NewWriter(file)
	n4, err := w.WriteString("buffered\n")
	check(err)
	fmt.Printf("wrote %d bytes\n", n4)

	err = w.Flush() // убеждаемся, что все буферизированные операции были применены к основному модулю записи
	check(err)
}
