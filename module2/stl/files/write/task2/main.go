package main

import (
	"os"
	"unicode"
	"unicode/utf8"
)

func toTranslit(s string) []byte {
	var res string
	convertT := map[rune]string{
		'а': "a",
		'б': "b",
		'в': "v",
		'г': "g",
		'д': "d",
		'е': "e",
		'ё': "yo",
		'ж': "zh",
		'з': "z",
		'и': "i",
		'й': "j",
		'к': "k",
		'л': "l",
		'м': "m",
		'н': "n",
		'о': "o",
		'п': "p",
		'р': "r",
		'с': "s",
		'т': "t",
		'у': "u",
		'ф': "f",
		'х': "h",
		'ц': "c",
		'ч': "ch",
		'ш': "sh",
		'щ': "sch",
		'ъ': "'",
		'ы': "y",
		'ь': "",
		'э': "e",
		'ю': "ju",
		'я': "ja",
		'А': "A",
		'Б': "B",
		'В': "V",
		'Г': "G",
		'Д': "D",
		'Е': "E",
		'Ё': "Yo",
		'Ж': "Zh",
		'З': "Z",
		'И': "I",
		'Й': "J",
		'К': "K",
		'Л': "L",
		'М': "M",
		'Н': "N",
		'О': "O",
		'П': "P",
		'Р': "R",
		'С': "S",
		'Т': "T",
		'У': "U",
		'Ф': "F",
		'Х': "H",
		'Ц': "C",
		'Ч': "Ch",
		'Ш': "Sh",
		'Щ': "Sch",
		'Э': "E",
		'Ю': "Ju",
		'Я': "Ja",
	}
	for len(s) > 0 {
		r, size := utf8.DecodeRuneInString(s)
		if unicode.Is(unicode.Cyrillic, r) {
			res += convertT[r]
		} else {
			res += s[:size]
		}
		s = s[size:]
	}
	return []byte(res)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data, err := os.ReadFile("module2/stl/files/write/task2/example.txt")
	check(err)
	data = toTranslit(string(data))
	err = os.WriteFile("module2/stl/files/write/task2/example.processed.txt", data, 0666)
	check(err)
}
