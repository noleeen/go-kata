package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}

}

func main() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')

	//вариант1 (практики ради):
	//err := os.WriteFile("module2/stl/files/write/name.txt", []byte(name), 0600)
	//check(err)

	file, err := os.Create("module2/stl/files/write/name.txt")
	check(err)

	writer := bufio.NewWriter(file)
	_, err = writer.WriteString(name)
	check(err)

	err = writer.Flush()
	check(err)

	err = file.Close()
	check(err)

	fmt.Printf("Hello %s\n", name)
}
