package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

//https://gobyexample.com.ru/reading-files тут с пояснениями

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data, err := os.ReadFile("t.txt")
	check(err)
	fmt.Println(string(data))

	file, err := os.Open("t.txt")
	check(err)
	defer func(file *os.File) {
		err := file.Close()
		check(err)
	}(file)

	b1 := make([]byte, 5)
	n1, err := file.Read(b1) //
	check(err)
	fmt.Printf("%b bytes: %s\n", n1, string(b1[:n1]))

	o2, err := file.Seek(6, 0) //начинаем с 6-го байта
	check(err)
	b2 := make([]byte, 2)
	n2, err := file.Read(b2)
	check(err)
	fmt.Printf("%d bytes @ %d: ", n2, o2)
	fmt.Printf("%v\n ", string(b2[:n2]))

	o3, err := file.Seek(7, 0)
	check(err)
	b3 := make([]byte, 4)
	n3, err := io.ReadAtLeast(file, b3, 2) // более надёжное считывание
	check(err)
	fmt.Printf("%d bytes @ %d: %s\n", n3, o3, string(b3))

	_, err = file.Seek(0, 0) //вернуться в начало файла
	check(err)

	r4 := bufio.NewReader(file)
	b4, err := r4.Peek(5)
	check(err)
	fmt.Printf("5 bytes: %s\n", string(b4))
}
