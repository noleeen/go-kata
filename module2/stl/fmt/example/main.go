package main

import "fmt"

type Person struct {
	name  string
	age   int
	money float64
}

func main() {
	story1 := Person{name: "Vlad", age: 25, money: 10.00000025}
	fmt.Printf("%v\n", story1)
	fmt.Printf("%+v\n", story1)
	fmt.Printf("%#v\n", story1)
	fmt.Printf("%T\n", story1)
	fmt.Printf("%t\n", true)
	fmt.Printf("%d\n", 123)
	fmt.Printf("%b\n", 12)
	fmt.Printf("%c\n", 33)
	fmt.Printf("%x\n", 456)
	fmt.Printf("%f\n", 4.56)
	fmt.Printf("%.3f\n", 4.56)
	fmt.Printf("%e\n", 123400000.0)
	fmt.Printf("%E\n", 123400000.0)
	fmt.Printf("%s\n", "\"string\"")
	fmt.Printf("%q\n", "\"string\"")
	fmt.Printf("%x\n", "hex this")
	fmt.Printf("%p\n", &story1)
	fmt.Printf("|%6d|%6d|\n", 12, 345)
	fmt.Printf("|%6.2f|%6.2f|\n", 1.2, 3.45)
	fmt.Printf("|%-6.2f|%-6.2f|\n", 1.2, 3.45)
	fmt.Printf("|%6s|%6s|\n", "foo", "b")
	fmt.Printf("|%-6s|%-6s|\n", "foo", "b")

}
