package main

import (
	"fmt"
	"math"
	"math/rand"

	"github.com/brianvoe/gofakeit/v6"
)

type Person struct {
	name  string
	age   int
	money float64
}

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf("Hello! My name is %s. I'm %d y. o. And I also have $%.2f in my wallet right now.", name, age, money)
}

func main() {
	story1 := Person{
		name:  "Vlad",
		age:   25,
		money: 10.00000025,
	}

	// вывод значений структуры
	fmt.Println("simple struct:", story1)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", story1)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", story1)

	var stories Person
	//l := stories.generateSelfStory(10)
	for i := 0; i < 100; i++ {
		stories.name = gofakeit.Name()
		stories.age = rand.Intn(62) + 18
		stories.money = math.Round(rand.Float64()*100)/100 + float64(rand.Intn(200))
		fmt.Println(generateSelfStory(stories.name, stories.age, stories.money))
	}

}
