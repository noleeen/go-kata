package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	buf := new(bytes.Buffer)
	// запишите данные в буфер
	for _, p := range data {
		p += "\n"
		_, err := buf.Write([]byte(p))
		if err != nil {
			fmt.Println("write error ")
		}
	}
	// создайте файл
	f, err := os.Create("module2/stl/io/homework/example.txt")
	if err != nil {
		fmt.Println("error create file ")
	}
	defer f.Close()

	// запишите данные в файл
	_, err = io.Copy(f, buf)
	if err != nil {
		fmt.Println("error: copy to file")
	}

	//прочтите данные в новый буфе
	buf2, err := os.ReadFile("module2/stl/io/homework/example.txt")
	if err != nil {
		fmt.Println("error: read data")
	}
	fmt.Println(string(buf2))

}
