package main

import (
	"fmt"
	"os"
)

func main() {
	proverbs := []string{
		"Channels orchestrate mutexes serialize",
		"Cgo is not Go",
		"Errors are values",
		"Don't panic",
	}
	f, err := os.Create("module2/stl/io/examples/writer/writeInFile/newF.txt")
	if err != nil {
		fmt.Println("err1")
		os.Exit(1)
	}
	defer f.Close()

	for _, p := range proverbs {
		p += "\n"
		n, err := f.Write([]byte(p))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if n != len(p) {
			fmt.Println("failed to write data")
			os.Exit(1)
		}
	}
	fmt.Println("file write done")

}
