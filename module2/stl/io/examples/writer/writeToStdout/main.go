package main

import (
	"fmt"
	"os"
)

func main() {
	proverbs := []string{
		"Channels orchestrate mutexes serialize",
		"Cgo is not Go",
		"Errors are values",
		"Don't panic",
	}
	for _, p := range proverbs {
		p += "\n"
		n, err := os.Stdout.Write([]byte(p))
		if err != nil {
			fmt.Println("err")
			os.Exit(1)
		}
		if n != len(p) {
			fmt.Println("failed to wtite data")
			os.Exit(1)
		}
	}

}
