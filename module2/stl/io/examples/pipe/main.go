package main

import (
	"bytes"
	"io"
	"os"
)

func main() {
	proverbs := new(bytes.Buffer)
	proverbs.WriteString("Channels orchestrate mutexes serialize\n")
	proverbs.WriteString("Cgo is not Go\n")
	proverbs.WriteString("Don't panic\n")

	pipeR, pipeW := io.Pipe()

	// write in writer end of pipe
	go func() {
		defer pipeW.Close()
		io.Copy(pipeW, proverbs)
	}()

	//read from reader end of pipe

	io.Copy(os.Stdout, pipeR)
	pipeR.Close()

}
