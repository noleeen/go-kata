package main

import (
	"fmt"
	"io"
)

type alphaReader struct {
	src string
	cur int
}

func newAlphaReader(src string) *alphaReader {
	return &alphaReader{src: src}
}

func alpha(r byte) byte {
	if (r >= 'A' && r <= 'Z') || (r >= 'a' && r <= 'z') {
		return r
	}
	return 0
}

func (a *alphaReader) Read(p []byte) (int, error) {
	if a.cur >= len(a.src) {
		return 0, io.EOF
	}

	x := len(a.src) - a.cur
	n, bound := 0, 0
	if x >= len(p) {
		bound = len(p)
	} else if x <= len(p) {
		bound = x
	}

	buf := make([]byte, bound)
	for n < bound {
		if char := alpha(a.src[a.cur]); char != 0 {
			buf[n] = char
		}
		n++
		a.cur++
	}
	copy(p, buf)
	return n, nil
}

func main() {
	// use an os.File as source for alphaReader
	//file, err := os.Open("./alpha_reader3.go")
	//if err != nil {
	//	fmt.Println(err)
	//	os.Exit(1)
	//}
	//defer file.Close()
	//reader := newAlphaReader(file)
	//p := make([]byte, 4)
	//for {
	//	n, err := reader.Read(p)
	//	if err == io.EOF {
	//		break
	//	}
	//	fmt.Print(string(p[:n]))
	//}
	//---------------------выше чтение из файла

	reader := newAlphaReader("Hello! It's 9am, where is the sun?")
	p := make([]byte, 4)
	for {
		n, err := reader.Read(p)
		if err == io.EOF {
			break
		}
		fmt.Print(string(p[:n]))
	}
	fmt.Println()
}
