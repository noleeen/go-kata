package main

import (
	"fmt"
	"reflect"
)

func main() {
	c := make(chan string, 2)
	vc := reflect.ValueOf(c)
	vc.Send(reflect.ValueOf("C"))
	succeeded := vc.TrySend(reflect.ValueOf("Go"))
	fmt.Println(succeeded) //true
	succeeded = vc.TrySend(reflect.ValueOf("C++"))
	fmt.Println(succeeded)          //false  (chan string, 2)
	fmt.Println(vc.Len(), vc.Cap()) //2 2
	vs, succeeded := vc.TryRecv()
	fmt.Println(vs.String(), succeeded) //C true
	vs, sentBeforeClosed := vc.Recv()
	fmt.Println(vs.String(), sentBeforeClosed) // Go true
	vs, succeeded = vc.TryRecv()
	fmt.Println(vs.String())
	fmt.Println(succeeded)
}
