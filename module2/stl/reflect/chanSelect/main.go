package main

import (
	"fmt"
	"reflect"
)

func main() {
	c := make(chan int, 1)
	vc := reflect.ValueOf(c)
	succeeded := vc.TrySend(reflect.ValueOf(123))
	fmt.Println(succeeded, vc.Len(), vc.Cap()) // true 1 1

	vSend, vZero := reflect.ValueOf(789), reflect.Value{}
	branches := []reflect.SelectCase{
		{Dir: reflect.SelectDefault, Chan: vZero, Send: vZero},
		{Dir: reflect.SelectRecv, Chan: vc, Send: vZero},
		{Dir: reflect.SelectSend, Chan: vc, Send: vSend},
	}
	selIndex, vRecv, sentBeforClosed := reflect.Select(branches)
	fmt.Println(selIndex)        //1
	fmt.Println(sentBeforClosed) //true
	fmt.Println(vRecv.Int())     //123
	vc.Close()
	//remove the send case branch this time,
	//for it may cause panic.
	selIndex, _, sentBeforClosed = reflect.Select(branches[:2])
	fmt.Println(selIndex, sentBeforClosed) // 1 false
}
