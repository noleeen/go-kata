package main

import (
	"fmt"
	"reflect"
)

func main() {
	type A = [16]int16
	var c <-chan map[A][]byte
	tc := reflect.TypeOf(c)
	fmt.Println(tc.Kind())
	fmt.Println(tc.ChanDir())
	tm := tc.Elem()
	ta, tb := tm.Key(), tm.Elem()
	fmt.Println(ta, tb, "\n", tm.Kind(), ta.Kind(), tb.Kind(), tc.Kind())
}
