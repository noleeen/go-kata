package main

//
//import (
//	"encoding/json"
//	"fmt"
//	"strings"
//)
//
//type Profile struct {
//	Username  string
//	Followers string
//}
//
//func (p *Profile) UnmarshalJSON(data []byte) error {
//	var container map[string]interface{}
//	_ = json.Unmarshal(data, &container)
//	fmt.Printf("container: %T / %#v\n\n", container, container)
//
//	// extract interface values
//	iuserName, _ := container["Username"]
//	ifollowers, _ := container["f_count"]
//	fmt.Printf("Username: %T/%#v\n", iuserName, iuserName)
//	fmt.Printf("followers: %T/%#v\n\n", ifollowers, ifollowers)
//
//	userName, _ := iuserName.(string)
//	followers, _ := ifollowers.(float64)
//	// extract concrete values
//	fmt.Printf("username: %T/%#v\n", userName, userName)
//	fmt.Printf("followers: %T/%#v\n\n", followers, followers)
//
//	// assign values
//	p.Username = strings.ToUpper(userName)
//	p.Followers = fmt.Sprintf("%.2fk", followers/1000)
//
//	return nil
//}
//
//type Students struct {
//	FirstNAme string
//	Profile   Profile
//}
//
//func main() {
//	data := []byte(`{
//		"FirstName": "Jo",
//		"Profile": {
//			"Username": "jo98",
//			"f_count": 1975
//		}
//	}`)
//
//	var john Students
//	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
//
//	fmt.Printf("%#v\n", john)
//}
