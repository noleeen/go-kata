package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}

type Student struct {
	FirstName, lastName string
	HeightInMeters      float64
	IsMale              bool
	Languages           [2]string
	Subjects            []string
	Grades              map[string]string
	Profile             *Profile
}

func main() {

	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": null,
		"Languages": [ "English" ],
		"Subjects": [ "Math", "Science" ],
		"Grades": null,
		"Profile": { "Followers": 1975 }
	}`)

	var john Student = Student{
		IsMale:    true,
		Languages: [2]string{"Korean", "Chinese"},
		Subjects:  nil,
		Grades:    map[string]string{"Math": "A"},
		Profile:   &Profile{Username: "johndoe91"},
	}

	fmt.Printf("Error: %v\n\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n\n", john)
	fmt.Printf("%#v\n", john.Profile)
	fmt.Println(john.lastName)
}

/*
Как видно из результата, значение поля массива Languages было переопределено значениями в JSON.
Поле Grades стало nil, потому что это поле map (и его нулевое значение равно nil) и значение в JSON равно null.

Поскольку поле Profile является указателем, а его значение в структуре john равно non-nil, Unmarshal использует
существующее значение указателя и назначенные значения поля из JSON. Если значение поля Profile в JSON было null,
то john было бы безоговорочно установлено значение поля Profile nil.
*/

//Если в Unmarshal встречается поле указателя и значение этого поля в JSON равно null,
//то этому полю присваивается значение указателя nil. Если поля в JSON не null,
//то для указателя выделяется новая память на случай,
//если указатель является nil либо повторно используется старое значение указателя.
