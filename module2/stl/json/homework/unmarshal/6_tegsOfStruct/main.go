package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string `json:"uname"`
	Followers int    `json:"f_count"`
}

type Student struct {
	FirstName      string   `json:"fname"`
	LastName       string   `json:"-"` // discard
	HeightInMeters float64  `json:"height"`
	IsMale         bool     `json:"male"`
	Languages      []string `json:",omitempty"`
	Profile        Profile  `json:"profile"`
}

func main() {

	data := []byte(`
	{
		"fname": "John",
		"LastName": "Doe",
		"height": 1.75,
		"IsMale": true,
		"Languages": null,
		"profile": {
			"uname": "johndoe91",
			"Followers": 1975
		}
	}`)

	var john Student = Student{
		Languages: []string{"English", "French"},
	}

	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n", john)
}

/*
Как видно из приведенного выше результата, поле IsMale в JSON не было рассортировано, потому что оно помечено именем поля male. То же самое происходит с вложенным полем Followers.

К сожалению, вариант omitempty не работает. Ты мог ожидать, что функция Unmarshal будет игнорировать поле, если его значение находится в JSON null, просматривая значение параметра omitempty, но, к сожалению, это не так. Будем надеяться, что Go рассмотрит возможность добавления этой функции в будущем (:
*/
