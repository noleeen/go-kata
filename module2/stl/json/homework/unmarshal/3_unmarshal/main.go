package main

import (
	"encoding/json"
	"fmt"
)

//JSON Unmarshal: обработка сложных данных

type Profile struct {
	Username  string
	Followers int
}

type Students struct {
	FirstName, lastName string
	HeightInMeters      float64
	IsMale              bool
	Languages           [2]string
	Subjects            []string
	Grades              map[string]string
	Profile             Profile
}

func main() {
	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": null,
		"Languages": [ "English", "Spanish", "German" ],
		"Subjects": [ "Math", "Science" ],
		"Grades": { "Math": "A" },
		"Profile": {
			"Username": "johndoe91",
			"Followers": 1975
		}
	}`)

	var john = Students{
		IsMale:   true,
		Subjects: []string{"Art"},
		Grades:   map[string]string{"Science": "A+"},
	}
	_ = json.Unmarshal(data, &john)
	fmt.Printf("%+v", john)
	fmt.Println(john.lastName)

}

/*
Поля, которые не экспортированы в structJSON или отсутствуют в JSON, не демаршалируются .
Если значение поля в JSON равно null и нулевое значение соответствующего типа поля равно nil,
(например, interface, map, pointer, или slice, то значение заменяется на nil,
в противном случае это поле игнорируется и сохраняет исходное значение.

Если Unmarshal встречается тип array и значения массива в JSON больше, чем может содержать массив,
лишние значения отбрасываются. Если значения массива в JSON меньше длины массива,
то остальные элементы массива устанавливаются в свои нулевые значения. Тип должен быть array совместим со значениями в JSON.

Если в Unmarshal встречается тип slice, то для slice в struct устанавливается нулевая длина,
а элементы из массива JSON добавляются по одному. Если JSON содержит пустой массив,
то Unmarshal заменяет slice в struct на пустой slice. Тип slice должен быть совместим со значениями в JSON.

Если в Unmarshal встречается тип map и значение карты в поле struct равно nil,
создается новый объект map и добавляются значения объекта в JSON.
Если значение map равно non-nil, то повторно используется исходное значение map и добавляются новые записи.
Тип map должен быть совместим со значениями в JSON.
*/
