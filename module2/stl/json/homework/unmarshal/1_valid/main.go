package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	data := []byte(`{
		"FirstName": "john",
		"Age": 21,
		"UsreName": "johndoe90",
		"Grades": null,
		"Languages": [
		"English",
		"French"
		]
	}`)

	isValid := json.Valid(data)
	fmt.Println(isValid)
}
