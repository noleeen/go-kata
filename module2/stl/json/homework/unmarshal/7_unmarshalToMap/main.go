package main

import (
	"encoding/json"
	"fmt"
)

type Student map[string]interface{}

func main() {

	data := []byte(`
	{
		"id": 123,
		"fname": "John",
		"height": 1.75,
		"male": true,
		"languages": null,
		"subjects": [ "Math", "Science" ],
		"profile": {
			"uname": "johndoe91",
			"f_count": 1975
		}
	}`)

	var john Student

	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n\n", john)

	i := 1
	for k, v := range john {
		fmt.Printf("%d: key (`%T`)`%v`, value (`%T`)`%#v`\n", i, k, k, v, v)
		i++
	}

	var kate interface{}
	//_ = json.Unmarshal(data, &kate)
	fmt.Println()
	fmt.Printf("%+v\n", kate)
	fmt.Printf("Before: `type` of `kate` is %T and its `value` is %v\n", kate, kate)

	// unmarshal `data`
	fmt.Printf("Error: %v\n", json.Unmarshal(data, &kate))
	fmt.Printf("After: `type` of `kate` is %T\n\n", kate)

	// print `kate` map
	fmt.Printf("%#v\n", kate)
	fmt.Println()
	fmt.Println()

	/* kate — это интерфейс, и для доступа к значениям map нам сначала нужно извлечь конкретное
	значение интерфейса. Для этого нужно использовать синтаксис утверждения типа interface.(Type).
	*/
	kateData := kate.(map[string]interface{})
	fmt.Println(kateData["fname"])
	//fmt.Println(kate["fname"]) ОШИБКА ! ! !
}
