package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	followers int
	Grabes    map[string]string
}

type Student struct {
	FirstName, lastNAme string
	Age                 int
	Profile             Profile
	Languages           []string
}

func main() {
	//var john Student

	john := Student{
		FirstName: "John",
		lastNAme:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johny3",
			followers: 1975,
			Grabes:    map[string]string{"Math": "A", "Science": "A+"},
		},
		Languages: []string{"English", "Italy"},
	}
	johnJSON, _ := json.Marshal(john)
	fmt.Printf("%#v", string(johnJSON))
}
