package main

import (
	"encoding/json"
	"fmt"
)

// Profile declares `Profile` structure
type Profile struct {
	Username  string
	followers int
	Grades    map[string]string
}

// Student declares `Student` structure
type Student struct {
	FirstName, lastName string
	Age                 int
	Profile             // АНОНИМНАЯ СТРУКТУРА (БЕЗ ИМЕНИ, ТОЛЬКО С ТИПОМ). ЕЁ ПОЛЯ  ПРОДВИГАЮТСЯ НА УРОВЕНЬ К ПОЛЯМ РОДИТЕЛЬСКОЙ СТРУКТУРЫ
	Languages           []string
}

func main() {

	// define `john` struct
	john := Student{
		FirstName: "John",
		lastName:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			followers: 1975,
		},
		Languages: []string{"English", "French"},
	}

	// encode `john` as JSON
	johnJSON, _ := json.MarshalIndent(john, "", "  ")

	// print JSON string
	fmt.Println(string(johnJSON))
}
