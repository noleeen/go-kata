package main

import (
	"encoding/json"
	"fmt"
)

type ProfileI interface {
	Follow()
}

type Profile struct {
	Username  string
	Followers int
}

func (p *Profile) Follow() {
	p.Followers++
}

type Students struct {
	FirstName, lastName string
	Age                 int
	Primary             ProfileI
	Secondary           ProfileI
}

func main() {
	john := &Students{
		FirstName: "John",
		lastName:  "Doe",
		Age:       21,
		Primary: &Profile{
			Username:  "jo34",
			Followers: 19,
		},
		Secondary: &Profile{
			Username:  "o",
			Followers: 20,
		},
	}

	fmt.Println(john.Primary)
	john.Primary.Follow() // вызываем метод нашего типа Profile
	fmt.Println(john.Primary)

	johnJSON, _ := json.MarshalIndent(john, "", "  ")
	//johnJSON, _ := json.Marshal(john)
	fmt.Println(john)

	fmt.Println(string(johnJSON))
}
