package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	UserName  string `json:"name"`
	Followers int    `json:"followers,omitempty,string"`
}

type Students struct {
	FirstName string  `json:"fname"`           //`fname` as FirstName
	LastName  string  `json:"lname,omitempty"` //discard if value is empty
	Email     string  `json:"-"`               //always discard
	Age       int     `json:"-,"`              //`-` as Age
	IsMale    bool    `json:",string"`         //keep original name and bool to string
	Profile   Profile `json:""`                // no effect
}

func main() {
	john := &Students{
		FirstName: "John",
		LastName:  "",
		Age:       21,
		Profile: Profile{
			UserName:  "jo1900",
			Followers: 1955,
		},
	}
	johnJSON, _ := json.MarshalIndent(john, "", "  ")
	fmt.Println(string(johnJSON))
}
