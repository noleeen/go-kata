package main

import (
	"encoding/json"
	"fmt"
)

type Student struct {
	FirstName, lastNAme string
	Email               string
	Age                 int
	HeightInMeters      float64
	IsMale              bool
}

func main() {
	john := Student{
		FirstName:      "John",
		lastNAme:       "Doe",
		Age:            21,
		HeightInMeters: 1.75,
		IsMale:         true,
	}

	johnJson, _ := json.Marshal(john)
	fmt.Println(string(johnJson))
}
