package main

import (
	"encoding/json"
	"fmt"
)

/*В приведенном выше примере тип структуры Profile реализует метод MarshalJSON, следовательно,
реализует интерфейс Marshaler. Когда Marshal встречается значение type Marshaler, он вызывает этот метод,
чтобы получить данные JSON (срез байтов) вместо маршалинга значения.

Мы также модифицировали поле Age типа структуры Student. Теперь он содержит значение типа Age, которое является
алиасом типа int. тип структуры Age реализует метод MarshalText, что означает, что он реализует
интерфейс TextMarshaler, определенный в пакете encoding.

Когда Marshal встречает значение типа TextMarshaler, он вызывает метод MarshalText и использует значение,
возвращаемое этим методом, для кодирования строкового значения JSON. Следовательно,
эта программа выдает следующий результат.
*/

type Profile struct {
	UserName  string
	Followers int
}

func (p Profile) MarshalJSON() ([]byte, error) {
	// return JSON value
	//DO: handle error gracefully
	return []byte(fmt.Sprintf(`{"f_count": "%d"}`, p.Followers)), nil
}

type Age int

func (a Age) MarshalText() (Text []byte, err error) {
	// return string value
	// DO: handle error gracefully
	return []byte(fmt.Sprintf(`{"age": %d}`, int(a))), nil
}

type Students struct {
	FirstName, lastName string
	Age                 Age
	Profile             Profile
}

func main() {
	john := &Students{
		FirstName: "Jo",
		lastName:  "df",
		Age:       Age(7),
		Profile: Profile{
			UserName:  "jo7",
			Followers: 201,
		},
	}
	johnJSON, _ := json.MarshalIndent(john, "", "  ")
	fmt.Println(string(johnJSON))

}
