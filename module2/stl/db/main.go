package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db, _ := sql.Open("sqlite3", "./gopher.db")
	statement, _ := db.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	_, err := statement.Exec()
	check(err)

	statement, _ = db.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	_, err = statement.Exec("Lorem", "Ipsum")
	check(err)

	rows, _ := db.Query("SELECT id, firstname, lastname FROM people")
	var id int
	var firstname string
	var lastname string

	for rows.Next() {
		err = rows.Scan(&id, &firstname, &lastname)
		check(err)
		fmt.Printf("%d, %s, %s\n", id, firstname, lastname)
	}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
