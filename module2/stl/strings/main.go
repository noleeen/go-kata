package main

import (
	"fmt"
	"strconv"
	"strings"
)

type Cache struct {
	data map[string]*User // можешь немного разъяснить зачем мы тут ссылку на тип указываем (если мы тут просто тип укажем и в методах и в var тоже соответственно меняем, то всё работает так же, set записывает get выводит....)?
}

func NewCache() *Cache {
	return &Cache{data: make(map[string]*User, 100)}
}

func (c *Cache) Set(key string, u *User) {
	c.data[key] = u
}

func (c *Cache) Get(key string) *User {
	return c.data[key]
}

type User struct {
	ID       int
	Nickname string
	Email    string
}

func main() {
	var users []*User
	emails := []string{"robpike@gmail.com", "davecheney@gmail.com", "bradfitzpatrick@gmail.com", "eliben@gmail.com", "quasilyte@gmail.com"}
	for i := range emails {
		users = append(users, &User{
			ID:       i + 1,
			Nickname: strings.Split(emails[i], "@")[0],
			Email:    emails[i],
		})
	}
	cache := NewCache()
	for i := range users {
		cache.Set(strings.Join([]string{users[i].Nickname, ":", strconv.Itoa(users[i].ID)}, ""), users[i])
	}
	keys := []string{"robpike:1", "davecheney:2", "bradfitzpatrick:3", "eliben:4", "quasilyte:5"}
	for i := range keys {
		fmt.Println(cache.Get(keys[i]))
	}
}
