package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"name"`
	Production bool
}

func main() {
	var configFile string
	flag.StringVar(&configFile, "conf", "", "file")
	flag.Parse()
	data, err := os.ReadFile(configFile)
	if err != nil {
		fmt.Println("read error")
	}

	conf := Config{}
	//fmt.Printf("Error: %+v\n", json.Unmarshal(data, &conf))
	_ = json.Unmarshal(data, &conf)
	fmt.Printf("Production: %v\nAppName: %v\n", conf.Production, conf.AppName)
}

//
//// Пример вывода конфига
//// Production: true
//// AppName: мое тестовое приложение
